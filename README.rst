=========================
The ``lino_book`` package
=========================





This is the code repository that contains (1) the Sphinx source files of the
Lino Developer Guide, (2) the ``lino_book`` Python package and (3) a test suite
with doctest-based tests for the Lino framework.

The **Lino Developer Guide** is the technical documentation tree of the Lino
framework.  It is visible on https://dev.lino-framework.org.

The ``lino_book`` Python package is a collection of small example Lino
applications used for educational and testing purposes.

The code repositories for the ``lino`` and ``lino_xl`` Python packages have no
documentation tree on their own and almost no unit tests, they are tested and
documented here.

- Code repository: https://gitlab.com/lino-framework/book
- Test results: https://gitlab.com/lino-framework/book/-/pipelines
- Feedback: https://community.lino-framework.org
- Maintainer: https://www.saffre-rumma.net/


