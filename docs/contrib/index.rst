.. _lino.contrib:

======================
Contribute to the code
======================

Until now we did not speak about how you can actively give back to Lino by
modifying some part of it. Here we go.

.. contents::
    :depth: 1
    :local:


Sharing code with others
========================

.. toctree::
   :maxdepth: 2

   /contrib/legal
   /team/pull_request
   /dev/team

QA and deployment
=================

.. toctree::
   :maxdepth: 2

   /dev/testing
   /dev/deploy
   /team/gh2gl

Writing documentation
=====================

.. toctree::
   :maxdepth: 2

   /writedocs/index
