.. doctest docs/dev/chooser2.rst

.. _book.dev.chooser2:

=====================
More chooser examples
=====================


.. contents::
   :depth: 1
   :local:

.. include:: /../docs/shared/include/tested.rst

>>> from lino import startup
>>> startup('lino_book.projects.noi1e.settings.demo')
>>> from lino.api.doctest import *

Choosers that need the requesting user
======================================

If your chooser method needs to know the current user to determine the choices
for a field, include a "ar" parameter to your chooser method:

.. literalinclude:: /../../book/lino_book/projects/chooser/ar_chooser.py

For example the chooser for the :attr:`lino_xl.lib.tickets.Ticket.site` field
wants to know who is asking before deciding which choices to display, because
not everybody can see every site.

>>> url = '/choices/tickets/Ticket/site'
>>> show_choices("robin", url) #doctest: +ELLIPSIS
<br/>
pypi
security

>>> show_choices("luc", url) #doctest: +ELLIPSIS
<br/>
docs
cust


Some special cases
==================

Asking choices for a field that doesn't exist:

>>> show_choices("robin", '/choices/tickets/Ticket/foo') #doctest: +ELLIPSIS
Traceback (most recent call last):
...
AttributeError: 'NoneType' object has no attribute 'blank'


Asking choices for a field that exists but has no choices:

>>> show_choices("robin", '/choices/tickets/Ticket/summary') #doctest: +ELLIPSIS
Traceback (most recent call last):
...
Exception: Response status (/choices/tickets/Ticket/summary) was 404 instead of 200

When asking the choices for a field on a slave table, you must specify the
master instance (even though it is not relevant):

>>> url = '/choices/tickets/TicketsByType/site?mk=1'
>>> show_choices("robin", url) #doctest: +ELLIPSIS
<br/>
pypi
security

When :attr:`lino.core.site.Site.strict_master_check` is True,
**not** specifying a master key when asking the choices for a field on a slave table
is an error:

>>> settings.SITE.strict_master_check = True
>>> url = '/choices/tickets/TicketsByType/site'
>>> show_choices("robin", url) #doctest: +ELLIPSIS
Traceback (most recent call last):
...
Exception: Response status (/choices/tickets/TicketsByType/site) was 400 instead of 200


:class:`lino.modlib.comments.CommentsByRFC` is a :term:`slave table` whose
master field is a GFK. In that case you must also specify the master *type*;
the master *key* is not enough.

>>> comments.CommentsByRFC.master_field  #doctest: +ELLIPSIS
<lino.modlib.gfks.fields.GenericForeignKey object at ...>

>>> comments.CommentsByRFC.master_field.name  #doctest: +ELLIPSIS
'owner'

A correct call:

.. preliminary test
  >>> contenttypes.ContentType.objects.get_for_model(tickets.Ticket).pk
  44

>>> url = '/choices/comments/Comments/owner_id?mk=12&mt=37&owner_type=44&limit=3'
>>> show_choices("robin", url) #doctest: +ELLIPSIS
<br/>
#1 (Föö fails to bar when baz)
#2 (Bar is not always baz)
#3 (Baz sucks)

A call without ``mt``:

>>> show_choices("robin", '/choices/comments/CommentsByRFC/reply_to?mk=12&limit=3')
Traceback (most recent call last):
...
Exception: Response status (/choices/comments/CommentsByRFC/reply_to?mk=12&limit=3) was 400 instead of 200

.. /choices/comments/CommentsByRFC/owner_id?limit=15&lv=1708267065.3505688&mk=5440&mt=36&owner_type=36&query=&rp=weak-key-0&start=0

The following snippet was used for reproducing #5440 (Changing the ticket of a
comment doesn't work):

>>> # comments.Comment.objects.filter(owner_id=116)

>>> comment = comments.Comment.objects.get(pk=501)
>>> comment.owner
Ticket #105 ('#105 (Irritating message when bar)')
>>> contenttypes.ContentType.objects.get_for_model(tickets.Ticket).pk
44
>>> comments.CommentsByRFC.master
<class 'django.contrib.contenttypes.models.ContentType'>

>>> url = '/choices/comments/CommentsByRFC/owner_id?mk=116&mt=44&owner_type=44&limit=3'
>>> show_choices("robin", url) #doctest: +ELLIPSIS
<br/>
#1 (Föö fails to bar when baz)
#2 (Bar is not always baz)
#3 (Baz sucks)
