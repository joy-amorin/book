===========
Get started
===========

This section describes how to install Lino on your computer and explore it.

.. toctree::
    :maxdepth: 1

    /dev/install/index
    /dev/hello/index
    /dev/env
    /dev/newbies/editor
    /discover
    /dev/myapp
    /dev/polls/index
