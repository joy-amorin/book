.. _dev.testing:

=========================
Testing Lino applications
=========================

You write test suites for a Lino application like for any other Django project.
But Lino adds a set of tools, conventions, ideas and suggestions for testing
your applications.


.. toctree::
   :maxdepth: 1

   runtests
   demotests
   doctests
   migtests
   cypress

.. _travis:

Travis CI
=========

The Lino team used to have an `account at Travis CI
<https://travis-ci.org/lino-framework/>`__ before we moved to GitLab.
