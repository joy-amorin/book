.. _dev.editor:

===================
Which editor to use
===================

Software developers spend most of their time inside a :term:`source code`
editor.

Don't waste too much of your time
with single-file editors like `joe
<https://en.wikipedia.org/wiki/Joe%27s_Own_Editor>`__ or `nano
<https://www.nano-editor.org/>`__. These are good for occasional changes in
files on a server that you access via a terminal, but they are not designed for
jumping back and forth in a repository with thousands of source code files.

If you haven't yet made up your choice about which editor to use, then we
recommend to start with Atom or its community-led successor Pulsar.  See next
section. There are other choices, most notably `PyCharm
<https://www.jetbrains.com/pycharm/>`__.

.. _atom:

Atom (Pulsar)
=============

Atom was developed by GitHub and discontinued when Microsoft bought them.
Although they want it to disappear, it is actually still there in most Linux
distributions. So you just say::

  $ sudo apt install atom

If this doesn't work, check out Pulsar https://pulsar-edit.dev/

Within Atom, you should install the `python-tools
<https://atom.io/packages/python-tools>`__ package and configure its "Path to
Python directory" to point to your :term:`default environment` (which you
installed in :doc:`/dev/install/index`).

Select :menuselection:`File --> Add project folder...` and add your
:xfile:`~/lino` directory. This will cause Atom to index all files below this
directory.

How to instruct Atom to use your :term:`default environment` when doing syntax
checks or finding definitions:

- Select :menuselection:`Edit --> Preferences --> Packages`

- Select the settings of the python-tools plugin

- Set the :guilabel:`Path to Python directory` field to :file:`~/lino/env/bin`
  (or whatever your chose as your :term:`default environment`).

Some useful keyboard shortcuts in Atom:

- :kbd:`Ctrl+P` open an existing file using fuzzy file name search within all files of the project.
- :kbd:`Shfit+Ctrl+F` find (and optionally replace) a text string in all files (or in some)
- :kbd:`Alt+Q` reflow selection
- :kbd:`Ctrl+Alt+G` go to definition

Other useful packages to install:

- `tidy-tabs <https://atom.io/packages/tidy-tabs>`__ causes Atom to close tabs
  that you haven't visited for some time. Useful because otherwise Atom can
  become a memory waster.
