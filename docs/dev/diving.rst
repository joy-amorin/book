.. _lino.dev:
.. _lino.quick.start:
.. _lino.user.start:
.. _dev.diving:

================
Dive into Lino
================

After having successfully installed your :term:`developer environment`, we are
now ready to dive *into* Lino.


The vital basics
================

.. toctree::
   :maxdepth: 1

   models
   tables/index
   layouts/index
   actions
   demo_fixtures
   choicelists
   perms
   nutshell

More basics
===========

.. toctree::
   :maxdepth: 1

   site
   plugins
   admin_main
   menu
   slave_tables
   layouts/more
   pyfixtures/index
   users
   sessions
   quantities
   xlmenu
   front_ends
   /dev/mldbc/index

Customize things
==================

.. toctree::
   :maxdepth: 1

   custom_actions
   create
   search
   format
   disable
   hide
   duplicate
   merge
   update
   delete

Widgets
=======

.. toctree::
   :maxdepth: 1

   combo/index
   textfield
   chooser2
   learningfk
   workflows
   radiobuttons
   virtualfields
   table_summaries
   vtables


More tutorials
==============

.. toctree::
   :maxdepth: 1

   screencasts
   /dev/lets/index
