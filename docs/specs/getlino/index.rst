.. _specs.getlino:

================================
``getlino`` : the Lino installer
================================

This section is not a usage guide. Instructions for
installing Lino are documented in different places depending on the context:

===============================  ================================
To install a...                   Read instructions at...
===============================  ================================
:term:`developer environment`     :ref:`getlino.install.dev`
:term:`production server`         :ref:`getlino.install.prod`
:term:`demo server`               :ref:`getlino.install.demo`
===============================  ================================

List of getlino commands:

.. toctree::
   :maxdepth: 1

   configure
   startsite
   list
   startproject

More about getlino:

.. toctree::
   :maxdepth: 2

   db
   misc
