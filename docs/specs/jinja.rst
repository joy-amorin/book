.. doctest docs/specs/jinja.rst
.. _specs.jinja:

===========================
``jinja`` : Jinja printing
===========================

.. currentmodule:: lino.modlib.jinja

This document describes the :mod:`lino.modlib.jinja` plugin

.. contents::
  :local:

.. include:: /../docs/shared/include/tested.rst

Code examples in this document use the :mod:`lino_book.projects.min1` demo
project:

>>> from lino import startup
>>> startup('lino_book.projects.min1.settings')
>>> from lino.api.doctest import *



.. class:: JinjaBuildMethod

  Inherits from :class:`lino.modlib.printing.DjangoBuildMethod`.

django-admin commands
=====================

This plugin defines two :cmd:`django-admin` commands.

.. management_command:: showsettings

Print to ``stdout`` all the Django settings that are active on this :term:`Lino
site`.

Usage example:

>>> from atelier.sheller import Sheller
>>> shell = Sheller("lino_book/projects/min1")
>>> shell("python manage.py showsettings | grep EMAIL")
... #doctest: +ELLIPSIS +NORMALIZE_WHITESPACE
DEFAULT_FROM_EMAIL = webmaster@localhost
EMAIL_BACKEND = django.core.mail.backends.smtp.EmailBackend
EMAIL_HOST = mail.example.com
EMAIL_HOST_PASSWORD =
EMAIL_HOST_USER =
EMAIL_PORT = 25
EMAIL_SSL_CERTFILE = None
EMAIL_SSL_KEYFILE = None
EMAIL_SUBJECT_PREFIX = [Django]
EMAIL_TIMEOUT = None
EMAIL_USE_LOCALTIME = False
EMAIL_USE_SSL = False
EMAIL_USE_TLS = False
SERVER_EMAIL = root@localhost


.. management_command:: status

Write a diagnostic status report about this :term:`Lino site`.

A functional replacement for the :manage:`diag` command.

>>> shell = Sheller("lino_book/projects/apc")
>>> shell("python manage.py status")
... #doctest: +ELLIPSIS +NORMALIZE_WHITESPACE +REPORT_UDIFF
Plugins
=======
<BLANKLINE>
- lino : lino
- about : lino.modlib.about
- jinja : lino.modlib.jinja(needed by lino.modlib.bootstrap3)
- bootstrap3 : lino.modlib.bootstrap3(needed by lino.modlib.extjs, needs ['lino.modlib.jinja'])
- extjs : lino.modlib.extjs(needs ['lino.modlib.bootstrap3'])
- printing : lino.modlib.printing(needed by lino.modlib.system)
- system : lino.modlib.system(needed by lino_cosi.lib.users, needs ['lino.modlib.printing'])
- users : lino_cosi.lib.users(needs ['lino.modlib.system'])
- contenttypes : django.contrib.contenttypes(needed by lino.modlib.gfks)
- gfks : lino.modlib.gfks(needs ['lino.modlib.system', 'django.contrib.contenttypes'])
- help : lino.modlib.help(needs ['lino.modlib.system'])
- office : lino.modlib.office(needed by lino_xl.lib.countries)
- xl : lino_xl.lib.xl(needed by lino_xl.lib.countries)
- countries : lino_xl.lib.countries(needs ['lino.modlib.office', 'lino_xl.lib.xl'])
- cosi : lino_cosi.lib.cosi(needed by lino_cosi.lib.contacts)
- contacts : lino_cosi.lib.contacts(needs ['lino_cosi.lib.cosi'])
- phones : lino_xl.lib.phones
- excerpts : lino_xl.lib.excerpts(needs ['lino.modlib.gfks', 'lino.modlib.printing', 'lino.modlib.office', 'lino_xl.lib.xl'])
- uploads : lino.modlib.uploads
- weasyprint : lino.modlib.weasyprint
- export_excel : lino.modlib.export_excel
- tinymce : lino.modlib.tinymce(needs ['lino.modlib.office'])
- accounting : lino_xl.lib.accounting(needed by lino_xl.lib.sepa, needs ['lino.modlib.weasyprint', 'lino_xl.lib.xl', 'lino.modlib.uploads'])
- sepa : lino_xl.lib.sepa(needs ['lino_xl.lib.accounting'])
- products : lino_cosi.lib.products(needs ['lino_xl.lib.xl'])
- memo : lino.modlib.memo(needed by lino_cosi.lib.trading, needs ['lino.modlib.office', 'lino.modlib.gfks'])
- linod : lino.modlib.linod(needed by lino.modlib.checkdata)
- checkdata : lino.modlib.checkdata(needed by lino_xl.lib.vat, needs ['lino.modlib.users', 'lino.modlib.gfks', 'lino.modlib.linod'])
- bevat : lino_xl.lib.bevat(needed by lino_xl.lib.vat, needs ['lino_xl.lib.vat'])
- vat : lino_xl.lib.vat(needed by lino_cosi.lib.trading, needs ['lino.modlib.checkdata', 'lino_xl.lib.excerpts'])
- trading : lino_cosi.lib.trading(needs ['lino.modlib.memo', 'lino_xl.lib.products', 'lino_xl.lib.vat'])
- invoicing : lino_xl.lib.invoicing(needs ['lino_xl.lib.trading'])
- finan : lino_xl.lib.finan(needs ['lino_xl.lib.accounting'])
- sheets : lino_xl.lib.sheets(needs ['lino_xl.lib.accounting'])
- staticfiles : django.contrib.staticfiles
- sessions : django.contrib.sessions
<BLANKLINE>
Config directories
==================
<BLANKLINE>
- .../lino_book/projects/apc/settings/config [writeable]
- .../lino_xl/lib/sheets/config
- .../lino_xl/lib/finan/config
- .../lino_xl/lib/trading/config
- .../lino_xl/lib/bevat/config
- .../lino_xl/lib/sepa/config
- .../lino_xl/lib/accounting/config
- .../lino/modlib/tinymce/config
- .../lino/modlib/weasyprint/config
- .../lino_xl/lib/excerpts/config
- .../lino_xl/lib/contacts/config
- .../lino/modlib/help/config
- .../lino/modlib/users/config
- .../lino/modlib/printing/config
- .../lino/modlib/extjs/config
- .../lino/modlib/bootstrap3/config
- .../lino/modlib/jinja/config
- .../lino/config



The output may be
customized by overriding the :xfile:`jinja/status.jinja.rst` template.

.. xfile:: jinja/status.jinja.rst

The template file used by the :manage:`status` command.
