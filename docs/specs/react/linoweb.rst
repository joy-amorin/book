.. doctest docs/specs/react/linoweb.rst
.. _specs.linoweb:

=======
linoweb
=======

>>> import lino
>>> lino.startup('lino_book.projects.noi1r.settings')
>>> from lino.api.doctest import *

>>> from django.contrib.staticfiles import finders
>>> from django.contrib.staticfiles.storage import staticfiles_storage

Define a utility function:

>>> def soupyfiy(url, Print=False):
...     r = test_client.get(url)
...     soup = BeautifulSoup(r.content, "lxml")
...     soup.body.hidden=True
...     if Print:
...         pSoup(soup)
...     return r,soup
>>> def pSoup(soup):
...     print(soup.body.prettify(formatter=None))
>>> test_client.force_login(rt.login('robin').user)
>>> rt.settings.SITE.build_site_cache(force=False)
... #doctest: +ELLIPSIS +REPORT_UDIFF +SKIP
2... lino*.js files have been built in ...

Static files
============

For some reason django test client doesn't find static files.
(Seems fixed since 20221019)

>>> test_client.get("/media/cache/js/lino_900_en.js")
... #doctest: +SKIP
Not Found: /media/cache/js/lino_900_en.js
<HttpResponseNotFound status_code=404, "text/html; charset=utf-8">


'TableRequest' object has no attribute 'obj2str'
================================================

The following snippets failed on 2023-02-16

>>> rt.login("robin").show(contacts.Roles)
==== ========== ===================== =====================
 ID   Function   Person                Organization
---- ---------- --------------------- ---------------------
 1    CEO        Annette Arens         Bäckerei Ausdemwald
 2    CEO        Erna Ärgerlich        Garage Mergelsberg
 3    CEO        Erna Ärgerlich        Rumma & Ko OÜ
 4               Andreas Arens         Rumma & Ko OÜ
 5               Annette Arens         Bäckerei Ausdemwald
 6               Hans Altenberg        Bäckerei Mießen
 7               Alfons Ausdemwald     Bäckerei Schmitz
 8               Laurent Bastiaensen   Garage Mergelsberg
==== ========== ===================== =====================
<BLANKLINE>

>>> erna = contacts.Role.objects.get(pk=2).person
>>> print(erna.pk)
168

The following snippet currently produces  **Oops, get_atomizer(contacts.Persons,
contacts.RolesByPerson, contacts.RolesByPerson) returned None** and we did not
yet find the reason. It is skipped only to get the CI green.

>>> url  = "values/contacts/Persons/{}/contacts.RolesByPerson".format(erna.pk)
>>> demo_get('robin', url, None, -1)  #doctest: +SKIP
GET /values/contacts/Persons/168/contacts.RolesByPerson for user Robin Rood got
{'data': '<div><p><a href="javascript:window.App.runAction({ '
         '&quot;actorId&quot;: &quot;contacts.Companies&quot;, &quot;an&quot;: '
         '&quot;detail&quot;, &quot;rp&quot;: null, &quot;status&quot;: { '
         '&quot;record_id&quot;: 104 } })">Garage Mergelsberg</a> '
         '(CEO)</p><p><a href="javascript:window.App.runAction({ '
         '&quot;actorId&quot;: &quot;contacts.Companies&quot;, &quot;an&quot;: '
         '&quot;detail&quot;, &quot;rp&quot;: null, &quot;status&quot;: { '
         '&quot;record_id&quot;: 100 } })">Rumma &amp; Ko OÜ</a> '
         '(CEO)</p></div>'}


ObjectDoesNotExist should not cause a traceback
===============================================

The following reaction is wrong. When some incoming requests specifies a
primary key that does not exist, this should return an error response, not cause
a traceback.

>>> url  = "values/contacts/Persons/5338/contacts.RolesByPerson"
>>> demo_get('robin', url)
Traceback (most recent call last):
...
django.core.exceptions.ObjectDoesNotExist: Invalid primary key 5338 for contacts.Persons
