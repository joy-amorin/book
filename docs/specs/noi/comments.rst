.. doctest docs/specs/noi/comments.rst
.. _noi.specs.comments:

==============================
``comments`` (comments in Noi)
==============================

.. currentmodule:: lino.modlib.comments


The :mod:`lino.modlib.comments` plugin in :ref:`noi` is configured and used to
satisfy the application requirements.

.. contents::
  :local:

.. include:: /../docs/shared/include/tested.rst

>>> from lino import startup
>>> startup('lino_book.projects.noi1e.settings.demo')
>>> from lino.api.doctest import *


Overview
========

Public comments in :ref:`noi` are visible even to anonymous users.

There are seven :class:`Commentable` models in :ref:`noi`, but only tickets have
a `CommentsByRFC` panel in their detail.

>>> pprint(list(rt.models_by_base(comments.Commentable)))
[<class 'lino_xl.lib.contacts.models.Company'>,
 <class 'lino_xl.lib.contacts.models.Partner'>,
 <class 'lino_noi.lib.contacts.models.Person'>,
 <class 'lino_noi.lib.groups.models.Group'>,
 <class 'lino_noi.lib.tickets.models.Site'>,
 <class 'lino_noi.lib.tickets.models.Ticket'>]

Whether a comment is private or not depends on its :term:`discussion topic`:
Comments on a ticket are public when neither the ticket nor its site are marked
private.

Comments are private by default:

>>> dd.plugins.comments.private_default
True

Comments on a team are public when the team is not private.

.. _dg.plugins.comments.visibility:

Visibility of comments
======================

The demo database contains 504 comments, 84 of which are about a ticket.
56 comments are public.

>>> comments.Comment.objects.all().count()
504
>>> comments.Comment.objects.filter(group__isnull=False).count()
84
>>> comments.Comment.objects.filter(group__isnull=False).first()
Comment #253 ('Comment #253')

>>> comments.Comment.objects.filter(ticket__isnull=False).count()
84
>>> comments.Comment.objects.filter(ticket__isnull=False).first()
Comment #421 ('Comment #421')
>>> comments.Comment.objects.filter(ticket=None).count()
420
>>> comments.Comment.objects.filter(private=False).count()
56
>>> comments.Comment.objects.filter(private=True).count()
448

>>> from django.db.models import Q
>>> rt.login("robin").show(comments.Comments,
...     column_names="id ticket__group user owner",
...     filter=Q(ticket__isnull=False),
...     limit=20, display_mode=((None, DISPLAY_MODE_TABLE),))
... #doctest: +ELLIPSIS +NORMALIZE_WHITESPACE +REPORT_UDIFF
===== ============ ================= =============================================
 ID    Team         Author            Topic
----- ------------ ----------------- ---------------------------------------------
 421                Jean              `#116 (Why is foo so bar) <…>`__
 422                Luc               `#116 (Why is foo so bar) <…>`__
 423                Marc              `#116 (Why is foo so bar) <…>`__
 424                Mathieu           `#116 (Why is foo so bar) <…>`__
 425                Romain Raffault   `#116 (Why is foo so bar) <…>`__
 426                Rolf Rompen       `#116 (Why is foo so bar) <…>`__
 427                Robin Rood        `#116 (Why is foo so bar) <…>`__
 428   Developers   Jean              `#115 (Cannot delete foo) <…>`__
 429   Developers   Luc               `#115 (Cannot delete foo) <…>`__
 430   Developers   Marc              `#115 (Cannot delete foo) <…>`__
 431   Developers   Mathieu           `#115 (Cannot delete foo) <…>`__
 432   Developers   Romain Raffault   `#115 (Cannot delete foo) <…>`__
 433   Developers   Rolf Rompen       `#115 (Cannot delete foo) <…>`__
 434   Developers   Robin Rood        `#115 (Cannot delete foo) <…>`__
 435                Jean              `#114 (No more foo when bar is gone) <…>`__
 436                Luc               `#114 (No more foo when bar is gone) <…>`__
 437                Marc              `#114 (No more foo when bar is gone) <…>`__
 438                Mathieu           `#114 (No more foo when bar is gone) <…>`__
 439                Romain Raffault   `#114 (No more foo when bar is gone) <…>`__
 440                Rolf Rompen       `#114 (No more foo when bar is gone) <…>`__
===== ============ ================= =============================================
<BLANKLINE>



Marc is a customer, so he can see only comments that are (1) public OR (2) his
own OR (3) about a group ("team") that he can see OR (4) about something that he
can see.

Comments in Noi can be about tickets or about groups.

- marc can see tickets that are (public OR his own) AND in a group that he can see
- marc can see groups that are (public OR of which he is a member)

>>> qs = rt.login('marc').spawn(comments.RecentComments).data_iterator
>>> printsql(qs)
... #doctest: +ELLIPSIS +NORMALIZE_WHITESPACE +REPORT_UDIFF
SELECT comments_comment.id,
       comments_comment.modified,
       comments_comment.created,
       comments_comment.body,
       comments_comment.body_short_preview,
       comments_comment.body_full_preview,
       comments_comment.user_id,
       comments_comment.owner_type_id,
       comments_comment.owner_id,
       comments_comment.reply_to_id,
       comments_comment.private,
       comments_comment.comment_type_id,
       COUNT(T5.id) AS num_replies,
       COUNT(comments_reaction.id) AS num_reactions
FROM comments_comment
LEFT OUTER JOIN groups_group ON (comments_comment.owner_id = groups_group.id
                                 AND (comments_comment.owner_type_id = 65))
LEFT OUTER JOIN tickets_ticket ON (comments_comment.owner_id = tickets_ticket.id
                                   AND (comments_comment.owner_type_id = 44))
LEFT OUTER JOIN comments_comment T5 ON (comments_comment.id = T5.reply_to_id)
LEFT OUTER JOIN comments_reaction ON (comments_comment.id = comments_reaction.comment_id)
WHERE ((NOT comments_comment.private
        OR comments_comment.user_id = 4)
       AND (groups_group.id IS NULL
            OR groups_group.id IN
              (SELECT DISTINCT U0.id
               FROM groups_group U0
               LEFT OUTER JOIN groups_membership U1 ON (U0.id = U1.group_id)
               WHERE (NOT U0.private
                      OR U1.user_id = 4)))
       AND (tickets_ticket.id IS NULL
            OR tickets_ticket.id IN
              (SELECT DISTINCT U0.id
               FROM tickets_ticket U0
               LEFT OUTER JOIN tickets_site U1 ON (U0.site_id = U1.id)
               LEFT OUTER JOIN groups_group U2 ON (U0.group_id = U2.id)
               LEFT OUTER JOIN groups_membership U3 ON (U2.id = U3.group_id)
               WHERE ((NOT U0.private
                       AND NOT U1.private)
                      OR U3.user_id = 4
                      OR U0.user_id = 4))))
GROUP BY comments_comment.id,
         comments_comment.modified,
         comments_comment.created,
         comments_comment.body,
         comments_comment.body_short_preview,
         comments_comment.body_full_preview,
         comments_comment.user_id,
         comments_comment.owner_type_id,
         comments_comment.owner_id,
         comments_comment.reply_to_id,
         comments_comment.private,
         comments_comment.comment_type_id
ORDER BY comments_comment.created DESC

>>> rt.login("robin").show(comments.RecentComments,
...     column_names="id ticket__group user owner",
...     filter=Q(ticket__isnull=False),
...     limit=10, display_mode=((None, DISPLAY_MODE_TABLE),))
... #doctest: +ELLIPSIS +NORMALIZE_WHITESPACE +REPORT_UDIFF
===== ========== ================= ============================================
 ID    Team       Author            Topic
----- ---------- ----------------- --------------------------------------------
 504   Managers   Robin Rood        `#105 (Irritating message when bar) <…>`__
 503   Managers   Rolf Rompen       `#105 (Irritating message when bar) <…>`__
 502   Managers   Romain Raffault   `#105 (Irritating message when bar) <…>`__
 501   Managers   Mathieu           `#105 (Irritating message when bar) <…>`__
 500   Managers   Marc              `#105 (Irritating message when bar) <…>`__
 499   Managers   Luc               `#105 (Irritating message when bar) <…>`__
 498   Managers   Jean              `#105 (Irritating message when bar) <…>`__
 497              Robin Rood        `#106 (How can I see where bar?) <…>`__
 496              Rolf Rompen       `#106 (How can I see where bar?) <…>`__
 495              Romain Raffault   `#106 (How can I see where bar?) <…>`__
===== ========== ================= ============================================
<BLANKLINE>


>>> rt.login("marc").show(comments.RecentComments,
...     column_names="id ticket__group user owner",
...     filter=Q(ticket__isnull=False),
...     limit=10, display_mode=((None, DISPLAY_MODE_TABLE),))
... #doctest: +ELLIPSIS +NORMALIZE_WHITESPACE +REPORT_UDIFF
===== ============ ================= ============================================
 ID    Team         Author            Topic
----- ------------ ----------------- --------------------------------------------
 504   Managers     Robin Rood        `#105 (Irritating message when bar) <…>`__
 503   Managers     Rolf Rompen       `#105 (Irritating message when bar) <…>`__
 502   Managers     Romain Raffault   `#105 (Irritating message when bar) <…>`__
 501   Managers     Mathieu           `#105 (Irritating message when bar) <…>`__
 500   Managers     Marc              `#105 (Irritating message when bar) <…>`__
 499   Managers     Luc               `#105 (Irritating message when bar) <…>`__
 498   Managers     Jean              `#105 (Irritating message when bar) <…>`__
 490   Sales team   Robin Rood        `#107 (Misc optimizations in Baz) <…>`__
 489   Sales team   Rolf Rompen       `#107 (Misc optimizations in Baz) <…>`__
 488   Sales team   Romain Raffault   `#107 (Misc optimizations in Baz) <…>`__
===== ============ ================= ============================================
<BLANKLINE>



Anonymous users they see only public comments.

>>> rt.show(comments.RecentComments,
...     column_names="id ticket__group user owner",
...     filter=Q(ticket__isnull=False),
...     limit=10, display_mode=((None, DISPLAY_MODE_TABLE),))
... #doctest: +ELLIPSIS +NORMALIZE_WHITESPACE +REPORT_UDIFF
===== ============ ================= ============================================
 ID    Team         Author            Topic
----- ------------ ----------------- --------------------------------------------
 504   Managers     Robin Rood        `#105 (Irritating message when bar) <…>`__
 503   Managers     Rolf Rompen       `#105 (Irritating message when bar) <…>`__
 502   Managers     Romain Raffault   `#105 (Irritating message when bar) <…>`__
 501   Managers     Mathieu           `#105 (Irritating message when bar) <…>`__
 500   Managers     Marc              `#105 (Irritating message when bar) <…>`__
 499   Managers     Luc               `#105 (Irritating message when bar) <…>`__
 498   Managers     Jean              `#105 (Irritating message when bar) <…>`__
 490   Sales team   Robin Rood        `#107 (Misc optimizations in Baz) <…>`__
 489   Sales team   Rolf Rompen       `#107 (Misc optimizations in Baz) <…>`__
 488   Sales team   Romain Raffault   `#107 (Misc optimizations in Baz) <…>`__
===== ============ ================= ============================================
<BLANKLINE>


>>> rows = []
>>> views = (comments.Comments, tickets.Tickets, groups.Groups)
>>> headers = ["User", "type"] + [i.__name__ for i in views]
>>> user_list = [users.User.get_anonymous_user()] + list(users.User.objects.all())
>>> for u in user_list:
...    cells = [str(u.username), u.user_type.name]
...    for dv in views:
...       qs = dv.request(user=u).data_iterator
...       cells.append(str(qs.count()))
...    rows.append(cells)
>>> print(rstgen.table(headers, rows))
... #doctest: +ELLIPSIS +NORMALIZE_WHITESPACE +REPORT_UDIFF
=========== ============= ========== ========= ========
 User        type          Comments   Tickets   Groups
----------- ------------- ---------- --------- --------
 anonymous   anonymous     35         48        0
 jean        developer     476        116       2
 luc         developer     504        116       3
 marc        customer      93         58        2
 mathieu     contributor   93         67        2
 romain      admin         504        116       3
 rolf        admin         504        116       3
 robin       admin         504        116       3
=========== ============= ========== ========= ========
<BLANKLINE>
