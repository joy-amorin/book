.. doctest docs/specs/noi/projects.rst
.. _noi.specs.projects:

==================
Project management
==================

.. contents::
  :local:

.. include:: /../docs/shared/include/tested.rst

>>> import lino
>>> lino.startup('lino_book.projects.noi1r.settings')
>>> from lino.api.doctest import *

The :class:`MySites` table will show only projects assigned to a team you are a
member of:

>>> ses = rt.login("robin")
>>> ses.show(tickets.MySites)
================== ============= ================================
 Project            Description   Workflow
------------------ ------------- --------------------------------
 `pypi <…>`__                     **⚒ Active** → [⚹] [☉] [☾] [☑]
 `security <…>`__                 **⚒ Active** → [⚹] [☉] [☾] [☑]
================== ============= ================================
<BLANKLINE>


>>> ses.show(groups.Memberships)
==== ================= ============ ========
 ID   User              Team         Remark
---- ----------------- ------------ --------
 1    Jean              Developers
 2    Luc               Managers
 3    Marc              Sales team
 4    Mathieu           Developers
 5    Romain Raffault   Managers
 6    Rolf Rompen       Sales team
 7    Robin Rood        Developers
==== ================= ============ ========
<BLANKLINE>

>>> ses.show(groups.MembershipsByUser, master_instance=ses.get_user())
`Robin Rood in Developers <…>`__, **New** **?**



>>> rt.login("robin").show(tickets.Sites, column_names="ref name id group")
=========== ============= ==== ============
 Reference   Designation   ID   Team
----------- ------------- ---- ------------
 admin       admin         6    Sales team
 bugs        bugs          3    Sales team
 cust        cust          5    Managers
 docs        docs          2    Managers
 pypi        pypi          1    Developers
 security    security      4    Developers
=========== ============= ==== ============
<BLANKLINE>



What is a project?
==================

.. currentmodule:: lino_xl.lib.tickets.models

.. class:: Project

    A **project** is something on which several users work together.

    A Project is something into which somebody (the `partner`) invests
    time, energy and money.  The partner can be either external or the
    runner of the site.

    Projects form a hierarchical tree: each Project can have a
    `parent` (another Project for which it is a sub-project).

    A project in Noi is called a *product backlog item* (PBI) or a
    *Sprint* in Scrum. (At least for the moment we don't see why Lino
    should introduce a new database model for differentiating them. We
    have the ProjectType

    .. attribute:: name

    .. attribute:: parent

    .. attribute:: assign_to

        The user to whom new tickets will be assigned.
        See :attr:`Ticket.assigned_to`.


Ticket versus project
=====================

The difference between "ticket" and "project" might not be obvious.
For example something that started as a seemingly meaningless "ticket"
can grow into a whole "project". But if this happens in reality, then
you simply do it in the database.

The most visible difference is that projects are handled by their
*name* while tickets just have a *number*.  Another rule of thumb is
that tickets are atomic tasks while projects are a way for grouping
tickets into a common goal. Tickets are short term while projects are
medium or long term. Tickets are individual and have a single author
while projects are group work. The only goal of a ticket is to get
resolved while a project has a more complex definition of goals and
requirements.



Project types
=============

.. class:: ProjectType

    The type of a :class:`Project`.


.. class:: TimeInvestment

    Model mixin for things which represent a time investment.  This
    currently just defines a group of three fields:

    .. attribute:: closed

        Whether this investment is closed, i.e. certain things should
        not change anymore.

    .. attribute:: private

        Whether this investment is private, i.e. should not be
        publicly visible anywhere.

        The default value is True.  Tickets on public projects cannot
        be private, but tickets on private projects may be manually
        set to public.

    .. attribute:: planned_time

        The time (in hours) we plan to work on this project or ticket.
