.. doctest docs/specs/noi/storage.rst
.. _noi.plugins.storage:

==================================
The ``storage`` plugin in Noi
==================================


.. contents::
   :local:
   :depth: 2

.. include:: /../docs/shared/include/tested.rst

>>> from lino import startup
>>> startup('lino_book.projects.noi1r.settings')
>>> from lino.api.doctest import *

>>> rt.show(storage.ProvisionStates)
... #doctest: +NORMALIZE_WHITESPACE +REPORT_UDIFF
======= =========== ===========
 value   name        text
------- ----------- -----------
 10      purchased   Purchased
======= =========== ===========
<BLANKLINE>

>>> rt.show(storage.Provisions)
... #doctest: +NORMALIZE_WHITESPACE +REPORT_UDIFF
==== ===================== ========= ================= ===========
 ID   Partner               Product   Provision state   qty
---- --------------------- --------- ----------------- -----------
 1    Rumma & Ko OÜ         Regular   Purchased         10:00
 2    Bäckerei Ausdemwald   Regular   Purchased         20:00
 3    Bäckerei Mießen       Regular   Purchased         19:28
 4    Bäckerei Schmitz      Regular   Purchased         11:12
 5    Garage Mergelsberg    Regular   Purchased         10:00
                                                        **70:40**
==== ===================== ========= ================= ===========
<BLANKLINE>


>>> rt.show(storage.Fillers)
==== ===================== ================= ================ =============== ============
 ID   Partner               Provision state   Wanted product   Minimum asset   Fill asset
---- --------------------- ----------------- ---------------- --------------- ------------
 1    Rumma & Ko OÜ         Purchased         Regular          2:00            10:00
 2    Bäckerei Ausdemwald   Purchased         Regular          2:00            20:00
 3    Bäckerei Mießen       Purchased         Regular          2:00            50:00
 4    Bäckerei Schmitz      Purchased         Regular          2:00            90:00
 5    Garage Mergelsberg    Purchased         Regular          2:00            10:00
==== ===================== ================= ================ =============== ============
<BLANKLINE>


>>> rt.show(storage.Components)
======================== ========= ===========
 Parent                   Child     qty
------------------------ --------- -----------
 Time credit (5 hours)    Regular   5:00
 Time credit (10 hours)   Regular   10:00
 Time credit (50 hours)   Regular   50:00
 **Total (3 rows)**                 **65:00**
======================== ========= ===========
<BLANKLINE>



Delivery notes
==============

>>> obj = rt.models.storage.DeliveryNote.objects.filter(partner=100).last()
>>> obj
DeliveryNote #107 ('SRV 13/2015')

>>> obj.partner
Partner #100 ('Rumma & Ko OÜ')

>>> rt.show('storage.ItemsByDeliveryNote', obj)
===================== ==================================== ========= =========== ====== ============================================
 No.                   Heading                              Product   Quantity    Move   Invoiced object
--------------------- ------------------------------------ --------- ----------- ------ --------------------------------------------
 1                     02.02.2015 12:58-13:10 Jean #31      Regular   0:12               `02.02.2015 12:58-13:10 Jean #31 <…>`__
 2                     04.02.2015 09:00-12:29 Jean #91      Regular   3:19               `04.02.2015 09:00-12:29 Jean #91 <…>`__
 3                     05.02.2015 10:02-13:01 Jean #1       Regular   2:49               `05.02.2015 10:02-13:01 Jean #1 <…>`__
 4                     09.02.2015 11:18-12:48 Jean #61      Regular   1:30               `09.02.2015 11:18-12:48 Jean #61 <…>`__
 5                     12.02.2015 09:00-12:53 Jean #31      Regular   3:43               `12.02.2015 09:00-12:53 Jean #31 <…>`__
 6                     13.02.2015 12:48-12:58 Jean #91      Regular   0:10               `13.02.2015 12:48-12:58 Jean #91 <…>`__
 7                     16.02.2015 12:29-13:06 Jean #1       Regular   0:37               `16.02.2015 12:29-13:06 Jean #1 <…>`__
 8                     18.02.2015 12:58-13:10 Jean #61      Regular   0:12               `18.02.2015 12:58-13:10 Jean #61 <…>`__
 9                     23.02.2015 09:00-10:02 Jean #31      Regular   1:02               `23.02.2015 09:00-10:02 Jean #31 <…>`__
 10                    25.02.2015 09:00-11:18 Jean #91      Regular   2:08               `25.02.2015 09:00-11:18 Jean #91 <…>`__
 11                    25.02.2015 12:58-15:00 Jean #1       Regular   1:52               `25.02.2015 12:58-15:00 Jean #1 <…>`__
 12                    03.02.2015 09:00-10:02 Mathieu #31   Regular   1:02               `03.02.2015 09:00-10:02 Mathieu #31 <…>`__
 13                    05.02.2015 09:00-11:18 Mathieu #91   Regular   2:08               `05.02.2015 09:00-11:18 Mathieu #91 <…>`__
 14                    05.02.2015 12:58-15:00 Mathieu #1    Regular   1:52               `05.02.2015 12:58-15:00 Mathieu #1 <…>`__
 15                    10.02.2015 09:00-12:53 Mathieu #61   Regular   3:43               `10.02.2015 09:00-12:53 Mathieu #61 <…>`__
 16                    12.02.2015 09:00-12:29 Mathieu #31   Regular   3:19               `12.02.2015 09:00-12:29 Mathieu #31 <…>`__
 17                    16.02.2015 12:53-12:58 Mathieu #91   Regular   0:05               `16.02.2015 12:53-12:58 Mathieu #91 <…>`__
 18                    17.02.2015 11:18-12:48 Mathieu #1    Regular   1:30               `17.02.2015 11:18-12:48 Mathieu #1 <…>`__
 19                    19.02.2015 09:00-10:02 Mathieu #61   Regular   1:02               `19.02.2015 09:00-10:02 Mathieu #61 <…>`__
 20                    23.02.2015 12:48-12:58 Mathieu #31   Regular   0:10               `23.02.2015 12:48-12:58 Mathieu #31 <…>`__
 21                    25.02.2015 10:02-13:01 Mathieu #91   Regular   2:49               `25.02.2015 10:02-13:01 Mathieu #91 <…>`__
 22                    26.02.2015 12:58-13:10 Mathieu #1    Regular   0:12               `26.02.2015 12:58-13:10 Mathieu #1 <…>`__
 **Total (22 rows)**                                                  **35:26**
===================== ==================================== ========= =========== ====== ============================================
<BLANKLINE>
