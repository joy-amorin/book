.. doctest docs/specs/noi/tickets.rst
.. _noi.specs.tickets:

======================================
``tickets`` (Ticket management in Noi)
======================================

The :mod:`lino_noi.lib.tickets` plugin extends :mod:`lino_xl.lib.tickets` to
make it collaborate with :mod:`lino_noi.lib.working`.

In :ref:`noi` the *site* of a *ticket* also indicates "who is going to pay" for
our work. Lino Noi uses this information when generating a service report.


.. currentmodule:: lino_noi.lib.tickets


.. contents::
  :local:

.. include:: /../docs/shared/include/tested.rst

>>> from lino import startup
>>> startup('lino_book.projects.noi1e.settings.demo')
>>> from lino.api.doctest import *


Tickets
=======

Here is a textual description of the fields and their layout used in
the detail window of a ticket.

>>> from lino.utils.diag import py2rst
>>> print(py2rst(tickets.AllTickets.detail_layout, True))
... #doctest: +ELLIPSIS +NORMALIZE_WHITESPACE +REPORT_UDIFF -SKIP
(main) [visible for all]:
- **General** (general_tab_1):
  - (general1): **None** (overview), **Add tag** (add_tag), **Tags** (topics.TagsByOwner) [visible for customer contributor developer admin]
  - (general2):
    - (general2_1): **Subscription** (order), **End user** (end_user), **Ticket type** (ticket_type)
    - (triager_panel_1) [visible for developer admin]: **Team** (group) [visible for all], **Assign to** (quick_assign_to) [visible for all]
    - (general2_3): **Priority** (priority), **Planned time** (planned_time), **Deadline** (deadline)
    - (general2_4): **Regular** (regular_hours), **Extra** (extra_hours), **Free** (free_hours)
    - **Sessions** (working.SessionsByTicket) [visible for contributor developer admin]
  - (general3): **Workflow** (workflow_buttons), **My comment** (comment), **Comments** (comments.CommentsByRFC)
- **More** (more_tab_1):
  - (more1):
    - (more1_1): **ID** (id), **Summary** (summary)
    - **My nickname** (my_nickname)
    - **Description** (description)
  - (more2): **Reference** (ref), **Resolution** (upgrade_notes)
  - (more3):
    - **State** (state)
    - **Assigned to** (assigned_to)
    - **Author** (user)
    - (more3_4): **Created** (created), **Modified** (modified)
    - **Confidential** (private)
    - **Upload files** (uploads.UploadsByController) [visible for customer contributor developer admin]
- **Links** (links_1):
  - (links1):
    - **Parent** (parent)
    - **Tickets** (TicketsByParent) [visible for customer contributor developer admin]
  - (links2): **Duplicate of** (duplicate_of), **Duplicates** (DuplicatesByTicket), **Mentioned in** (comments.CommentsByMentioned)
<BLANKLINE>


.. class:: Ticket

    The Django model used to represent a *ticket* in Noi. Adds some fields and
    methods.

    .. attribute:: assigned_to

        The user who is working on this ticket.

    .. attribute:: site

        The site this ticket belongs to.
        You can select only sites you are subscribed to.


Screenshots
===========

.. image:: tickets.Ticket.merge.png


The life cycle of a ticket
==========================

In :ref:`noi` we use the default tickets workflow defined  in
:class:`lino_xl.lib.tickets.TicketStates`.


Projects
========

The list of projects in our demo database depends on who is looking at it.
Anonymous users can see only public projects:

>>> rt.show(tickets.Sites)
=========== ============= ======== ============== ====
 Reference   Designation   Remark   Workflow       ID
----------- ------------- -------- -------------- ----
 admin       admin                  **⚒ Active**   6
 bugs        bugs                   **⚒ Active**   3
 cust        cust                   **⚒ Active**   5
 docs        docs                   **⚒ Active**   2
 pypi        pypi                   **⚒ Active**   1
=========== ============= ======== ============== ====
<BLANKLINE>


>>> rt.login("marc").show(tickets.Sites)
=========== ============= ======== ================================ ====
 Reference   Designation   Remark   Workflow                         ID
----------- ------------- -------- -------------------------------- ----
 admin       admin                  **⚒ Active** → [⚹] [☉] [☾] [☑]   6
 bugs        bugs                   **⚒ Active** → [⚹] [☉] [☾] [☑]   3
 cust        cust                   **⚒ Active** → [⚹] [☉] [☾] [☑]   5
 docs        docs                   **⚒ Active** → [⚹] [☉] [☾] [☑]   2
 pypi        pypi                   **⚒ Active** → [⚹] [☉] [☾] [☑]   1
=========== ============= ======== ================================ ====
<BLANKLINE>

List of projects in which Jean is interested (i.e. that are assigned to a team
where Jean is member):

>>> rt.login("jean").show(tickets.MySites)
================== ============= ================================
 Project            Description   Workflow
------------------ ------------- --------------------------------
 `pypi <…>`__                     **⚒ Active** → [⚹] [☉] [☾] [☑]
 `security <…>`__                 **⚒ Active** → [⚹] [☉] [☾] [☑]
================== ============= ================================
<BLANKLINE>


List of tickets that have not yet been assigned to a project:

>>> pv = dict(has_site=dd.YesNo.no)
>>> rt.login("robin").show(tickets.AllTickets, param_values=pv)
... #doctest: -REPORT_UDIFF +ELLIPSIS
===== =========================================== ========================================================== ======
 ID    Summary                                     Workflow                                                   Team
----- ------------------------------------------- ---------------------------------------------------------- ------
 116   Why is foo so bar                           [✋] [▶] **☒ Refused** → [⚹] [☾] [☎] [☉]
 114   No more foo when bar is gone                [✋] [▶] **☐ Ready** → [⚹] [☾] [☎] [☑] [☒]
 112   How to get bar from foo                     [✋] [▶] **⚒ Working** → [⚹] [☾] [☎] [☉] [☐] [☑] [☒] [⧖]
 110   Bar cannot baz                              [✋] [▶] **☎ Talk** → [⚹] [☾] [☉] [☐] [☑] [☒] [⧖]
 108   Default account in invoices per partner     [✋] [▶] **⧖ Waiting** → [⚹] [☾] [☎] [☉]
 ...
 10    Where can I find a Foo when bazing Bazes?   [✋] [▶] **⚹ New** → [☾] [☎] [☉] [☐] [☑] [⧖]
 8     Is there any Bar in Foo?                    [✋] [▶] **☒ Refused** → [⚹] [☾] [☎] [☉]
 6     Sell bar in baz                             [✋] [▶] **☐ Ready** → [⚹] [☾] [☎] [☑] [☒]
 4     Foo and bar don't baz                       [✋] [▶] **⚒ Working** → [⚹] [☾] [☎] [☉] [☐] [☑] [☒] [⧖]
 2     Bar is not always baz                       [✋] [▶] **☎ Talk** → [⚹] [☾] [☉] [☐] [☑] [☒] [⧖]
===== =========================================== ========================================================== ======
<BLANKLINE>



Ticket types
============

The :fixture:`demo` fixture defines the following ticket types.

>>> rt.show(tickets.TicketTypes)
============= ================== ================== ================
 Designation   Designation (de)   Designation (fr)   Reporting type
------------- ------------------ ------------------ ----------------
 Bugfix        Bugfix             Bugfix             Regular
 Enhancement   Enhancement        Enhancement        Extra
 Upgrade       Upgrade            Upgrade            Regular
============= ================== ================== ================
<BLANKLINE>

Deciding what to do next
========================

Show all active tickets reported by me.

>>> rt.login('marc').show(tickets.MyTicketsToWork, max_width=40)
... #doctest: -REPORT_UDIFF
+----------+------------------------------------------+------------------------------------------+
| Priority | Ticket                                   | Workflow                                 |
+==========+==========================================+==========================================+
| 30       | `#101 (Foo never bars) <…>`__ (by `Marc  | **☎ Talk** → [⚹] [☾] [☉] [⚒] [☐] [☑] [☒] |
|          | <…>`__ in `Sales team <…>`__)            | [⧖]                                      |
+----------+------------------------------------------+------------------------------------------+
| 30       | `#83 (Misc optimizations in Baz) <…>`__  | **☎ Talk**                               |
|          | (by `Rolf Rompen <…>`__ in `Sales team   |                                          |
|          | <…>`__)                                  |                                          |
+----------+------------------------------------------+------------------------------------------+
<BLANKLINE>



>>> rt.login('jean').show(tickets.MyTickets, max_width=30)
... #doctest: -REPORT_UDIFF
+----------+--------------------------------+-----------------+--------------------------------+
| Priority | Ticket                         | Assigned to     | Workflow                       |
+==========+================================+=================+================================+
| 30       | `#92 (Why is foo so bar)       |                 | [✋] [▶] **☎ Talk** → [⚹] [☾]  |
|          | <…>`__                         |                 | [☉] [☐] [☑] [☒] [⧖]            |
+----------+--------------------------------+-----------------+--------------------------------+
| 30       | `#85 ('NoneType' object has no | Mathieu         | [▶] **⚒ Working** → [⚹] [☾]    |
|          | attribute 'isocode') <…>`__    |                 | [☎] [☉] [☐] [☑] [☒] [⧖]        |
|          | (in `Developers <…>`__)        |                 |                                |
+----------+--------------------------------+-----------------+--------------------------------+
| 30       | `#78 (No more foo when bar is  |                 | [✋] [▶] **☐ Ready** → [⚹] [☾] |
|          | gone) <…>`__                   |                 | [☎] [☑] [☒]                    |
+----------+--------------------------------+-----------------+--------------------------------+
| 30       | `#64 (How to get bar from foo) |                 | [✋] [▶] **⚹ New** → [☾] [☎]   |
|          | <…>`__                         |                 | [☉] [☐] [☑] [⧖]                |
+----------+--------------------------------+-----------------+--------------------------------+
| 30       | `#57 (Irritating message when  |                 | [✋] [▶] **☉ Open** → [⚹] [☾]  |
|          | bar) <…>`__ (in `Managers      |                 | [☎] [⚒] [☐] [☑] [☒] [⧖]        |
|          | <…>`__)                        |                 |                                |
+----------+--------------------------------+-----------------+--------------------------------+
| 30       | `#29 (Foo never bars) <…>`__   | Rolf Rompen     | [▶] **☎ Talk** → [⚹] [☾] [☉]   |
|          | (in `Sales team <…>`__)        |                 | [⚒] [☐] [☑] [☒] [⧖]            |
+----------+--------------------------------+-----------------+--------------------------------+
| 30       | `#22 (How can I see where      |                 | [✋] [▶] **⚒ Working** → [⚹]   |
|          | bar?) <…>`__                   |                 | [☾] [☎] [☉] [☐] [☑] [☒] [⧖]    |
+----------+--------------------------------+-----------------+--------------------------------+
| 30       | `#15 (Bars have no foo) <…>`__ | Romain Raffault | [▶] **☐ Ready** → [⚹] [☾] [☎]  |
|          | (in `Managers <…>`__)          |                 | [☑] [☒]                        |
+----------+--------------------------------+-----------------+--------------------------------+
| 30       | `#1 (Föö fails to bar when     |                 | [✋] [▶] **⚹ New** → [☾] [☎]   |
|          | baz) <…>`__ (in `Developers    |                 | [☉] [⚒] [☐] [☑] [⧖]            |
|          | <…>`__)                        |                 |                                |
+----------+--------------------------------+-----------------+--------------------------------+
<BLANKLINE>




The backlog
===========

The :class:`TicketsBySite` panel shows all the tickets for a given project. It
is a scrum backlog.

>>> pypi = tickets.Site.objects.get(ref="pypi")
>>> rt.login("robin").show(tickets.TicketsBySite, pypi, max_width=40)
... #doctest: -REPORT_UDIFF -SKIP +ELLIPSIS +NORMALIZE_WHITESPACE
+--------------------+------------------------------------------+------------------------------------------+--------------+------------+-------+------------+
| Priority           | Ticket                                   | Workflow                                 | Planned time | Regular    | Extra | Free       |
+====================+==========================================+==========================================+==============+============+=======+============+
| 30                 | `#109 ('NoneType' object has no          | [▶] **⚹ New** → [☾] [☎] [☉] [⚒] [☐] [☑]  |              | 124:17     |       |            |
|                    | attribute 'isocode') <…>`__ (by `Mathieu  | [⧖]                                      |              |            |       |            |
|                    | <…>`__ in `Developers <…>`__)              |                                          |              |            |       |            |
+--------------------+------------------------------------------+------------------------------------------+--------------+------------+-------+------------+
| 30                 | `#85 ('NoneType' object has no attribute | [▶] **⚒ Working** → [⚹] [☾] [☎] [☉] [☐]  |              | 122:33     |       |            |
|                    | 'isocode') <…>`__ (by `Jean <…>`__ in      | [☑] [☒] [⧖]                              |              |            |       |            |
|                    | `Developers <…>`__)                       |                                          |              |            |       |            |
+--------------------+------------------------------------------+------------------------------------------+--------------+------------+-------+------------+
| 30                 | `#73 ('NoneType' object has no attribute | [✋] [▶] **⚹ New** → [☾] [☎] [☉] [⚒] [☐] |              | 126:53     |       |            |
|                    | 'isocode') <…>`__ (by `Marc <…>`__ in      | [☑] [⧖]                                  |              |            |       |            |
|                    | `Developers <…>`__)                       |                                          |              |            |       |            |
+--------------------+------------------------------------------+------------------------------------------+--------------+------------+-------+------------+
| 30                 | `#49 ('NoneType' object has no attribute | [✋] [▶] **⚒ Working** → [⚹] [☾] [☎] [☉] |              | 127:09     |       |            |
|                    | 'isocode') <…>`__ (by `Robin Rood <…>`__   | [☐] [☑] [☒] [⧖]                          |              |            |       |            |
|                    | in `Developers <…>`__)                    |                                          |              |            |       |            |
+--------------------+------------------------------------------+------------------------------------------+--------------+------------+-------+------------+
| 30                 | `#37 ('NoneType' object has no attribute | [▶] **⚹ New** → [☾] [☎] [☉] [⚒] [☐] [☑]  |              | 124:41     |       | 174:46     |
|                    | 'isocode') <…>`__ (by `Luc <…>`__ in       | [⧖]                                      |              |            |       |            |
|                    | `Developers <…>`__)                       |                                          |              |            |       |            |
+--------------------+------------------------------------------+------------------------------------------+--------------+------------+-------+------------+
| 30                 | `#13 (Bar cannot foo) <…>`__ (by `Rolf    | [▶] **⚒ Working** → [⚹] [☾] [☎] [☉] [☐]  |              | 125:40     |       | 168:57     |
|                    | Rompen <…>`__ in `Developers <…>`__)       | [☑] [☒] [⧖]                              |              |            |       |            |
+--------------------+------------------------------------------+------------------------------------------+--------------+------------+-------+------------+
| 30                 | `#1 (Föö fails to bar when baz) <…>`__    | [✋] [▶] **⚹ New** → [☾] [☎] [☉] [⚒] [☐] |              | 125:31     |       | 168:17     |
|                    | (by `Jean <…>`__ in `Developers <…>`__)    | [☑] [⧖]                                  |              |            |       |            |
+--------------------+------------------------------------------+------------------------------------------+--------------+------------+-------+------------+
| **Total (7 rows)** |                                          |                                          |              | **876:44** |       | **512:00** |
+--------------------+------------------------------------------+------------------------------------------+--------------+------------+-------+------------+
<BLANKLINE>


Anonymous cannot see tickets of a non-public project.

>>> security = tickets.Site.objects.get(ref="security")
>>> rt.show(tickets.TicketsBySite, security)
... #doctest: +REPORT_UDIFF -SKIP +ELLIPSIS +NORMALIZE_WHITESPACE
No data to display


Links between tickets
=====================

>>> obj = tickets.Ticket.objects.get(id=20)
>>> rt.show(tickets.TicketsByParent, obj)
... #doctest: +REPORT_UDIFF
========== ==== ============================= =================
 Priority   ID   Summary                       Assign to
---------- ---- ----------------------------- -----------------
 30         21   Irritating message when bar   luc, **romain**
========== ==== ============================= =================
<BLANKLINE>


Filtering tickets
=================

:ref:`noi` modifies the list of the parameters you can use for filterings
tickets be setting a custom :attr:`params_layout`.

>>> show_fields(tickets.AllTickets, all=True)
... #doctest: +ELLIPSIS +NORMALIZE_WHITESPACE +REPORT_UDIFF
- Author (user) : The author or reporter of this ticket. The user who reported this
  ticket to the database and is responsible for managing it.
- End user (end_user) : Only rows concerning this end user.
- Assigned to (assigned_to) : Only tickets with this user assigned.
- Not assigned to (not_assigned_to) : Only that this user is not assigned to.
- Interesting for (interesting_for) : Only tickets interesting for this partner.
- Project (site) : Show only tickets within this project.
- Has site (has_site) : Show only (or hide) tickets which have a site assigned.
- State (state) : Only rows having this state.
- Assigned (show_assigned) : Show only (or hide) tickets that are assigned to somebody.
- Active (show_active) : Show only (or hide) tickets which are active (i.e. state is Talk
  or ToDo).
- To do (show_todo) : Show only (or hide) tickets that are todo (i.e. state is New
  or ToDo).
- Private (show_private) : Show only (or hide) tickets that are marked private.
- Date from (start_date) : Start of observed date range
- until (end_date) : End of observed date range
- Observed event (observed_event) :
- Has reference (has_ref) :
- Commented Last (last_commenter) : Only tickets that have this use commenting last.
- Not Commented Last (not_last_commenter) : Only tickets where this use is not the last commenter.



Change observers
================

A comment is a :class:`ChangeNotifier` that forwards its owner's change
observers:

>>> ar = rt.login('robin')

>>> from lino.modlib.notify.mixins import ChangeNotifier
>>> obj = comments.Comment.objects.filter(group__isnull=False).first()
>>> obj.owner
Group #3 ('Sales team')
>>> isinstance(obj.owner, ChangeNotifier)
True
>>> list(obj.get_change_observers())
... #doctest: +REPORT_UDIFF -SKIP +ELLIPSIS +NORMALIZE_WHITESPACE
[(User #4 ('Marc'), <notify.MailModes.often:often>),
 (User #2 ('Rolf Rompen'), <notify.MailModes.often:often>)]

>>> list(obj.get_change_observers()) == list(obj.owner.get_change_observers())
True

When the owner of a comment is not a ChangeNotifier, the comment has no change
observers:

>>> obj = comments.Comment.objects.filter(group__isnull=True).first()
>>> obj.owner
Company #181 ('Saffre-Rumma')
>>> isinstance(obj.owner, ChangeNotifier)
False
>>> list(obj.get_change_observers())
[]
>>> list(obj.owner.get_change_observers())
Traceback (most recent call last):
...
AttributeError: 'Company' object has no attribute 'get_change_observers'



>>> list(comments.Comment.objects.get(pk=155).get_change_observers())
... #doctest: +REPORT_UDIFF -SKIP +ELLIPSIS +NORMALIZE_WHITESPACE
[]

Don't read on
=============

>>> print(tickets.Ticket.objects.get(pk=45))
#45 (Irritating message when bar)

>>> test_client.force_login(rt.login('robin').user)
>>> def mytest(k):
...     # url = 'http://jane.mylino.net/#/api/tickets/Tickets/{}?dm=list&fmt=json&lv=1697917143.868&mjsts=1695264043.076&mk=0&pv=1&pv&pv&pv&pv&pv&pv&pv&pv&pv&pv&pv&pv&pv=24.10.2023&pv=24.10.2023&pv=10&pv&pv&pv&rp=weak-key-4&ul=en&wt=d'.format(k)
...     url = 'http://jane.mylino.net/api/tickets/Tickets/{}?dm=list&fmt=json&lv=1697917143.868&mjsts=1695264043.076&mk=0&pv=1&pv&pv&pv&pv&pv&pv&pv&pv&pv&pv&pv&pv&pv=24.10.2023&pv=24.10.2023&pv=10&pv&pv&pv&rp=weak-key-4&ul=en&wt=d'.format(k)
...     res = test_client.get(url)
...     print(res)

>>> mytest("45")  #doctest: -SKIP
Traceback (most recent call last):
...
lino.core.exceptions.UnresolvedChoice: Unresolved value '10' (<class 'str'>) for tickets.TicketEvents (set Site.strict_choicelist_values to False to ignore this)
