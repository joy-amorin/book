.. _voga.specs:
.. _voga.tested:

===============
Lino Voga specs
===============

This section contains articles which are meant as technical
specifications. One of their goal is to get tested.


.. toctree::
   :maxdepth: 1

   general
   usertypes
   voga
   accounting
   courses
   presences
   cal
   holidays
   invoicing
   trading
   checkdata
   partners
   print_labels
   pupils
   db_roger
