==============
Shell commands
==============


.. command:: django-admin

    A command that gets installed with Django.
    See the Django docs about `django-admin and manage.py
    <https://docs.djangoproject.com/en/5.0/ref/django-admin/>`__ and `Writing
    custom django-admin commands
    <https://docs.djangoproject.com/en/5.0/howto/custom-management-commands/>`_

.. command:: inv

  This gets installed by `invoke <https://www.pyinvoke.org/index.html>`__ (which
  gets installed by :mod:`atelier`, which gets installed when you :doc:`install
  your developer environment </dev/install/index>`)).
