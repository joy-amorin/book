.. _specs:
.. _book.specs:

===================
Reference
===================

..
  This section of the :term:`developer documentation`
  is an entry point via structural entities or component.
  If you know the "key", then here is where you can look it up to get your answers.

.. toctree::
   :maxdepth: 1

   /changes/index
   /genindex
   /plugins/index
   /projects/index
   /apps/index
   commands/index
   settings
   commands/misc
   fields/index
   API </api/index>
   demo_fixtures
   /about/overview

Miscellaneous reference pages
=============================

.. toctree::
   :maxdepth: 1

   glossary
   links
   files
   tools
   javascript
   jargon
   obsolete
