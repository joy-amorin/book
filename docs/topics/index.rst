.. _dev.acquainted:

======
Topics
======

.. contents::
   :depth: 1
   :local:

Python & Django
===============

.. toctree::
  :maxdepth: 1

  /specs/gfktest
  /specs/migrate
  /specs/dpy
  /topics/utils
  polymorphism
  startup
  formatting
  html
  datetime
  m2m
  db
  atomizer
  layouts

Actors and actions
==================

.. toctree::
   :maxdepth: 1

   /dev/actors
   /dev/parameters
   /dev/ar
   /dev/action_parameters
   perms
   /dev/crud
   singlerow

Database
========

.. toctree::
   :maxdepth: 1

   /dev/datamig
   /dev/querysets
   /dev/gfks
   /dev/remote_fields
   /dev/inject_field
   /specs/projects/mti
   /specs/projects/nomti
   /dev/delete
   /dev/auto_create
   /dev/mti
   model
   sql


.. _dg.topics.printing:

Printing
========

.. toctree::
   :maxdepth: 1

   /dev/printing
   /dev/cache
   /dev/rendering
   templates


Contacts & calendar
===================

.. toctree::
   :maxdepth: 1

   /specs/human
   /specs/born
   /utils/addressable
   /specs/holidays
   beid

Business
==========

.. toctree::
   :maxdepth: 1

   /specs/accounting
   /specs/iban
   vies
   pos


The web front end
=================

.. toctree::
   :maxdepth: 1

   /specs/invalid_requests
   /specs/delayed_values
   /specs/ajax
   /specs/jsgen
   /specs/html
   /specs/react/index

See also the `React Developer Guide
<https://lino-framework.gitlab.io/react/guide/index.html>`__

.. _dg.topics.plugins:

About plugins
=================

.. toctree::
   :maxdepth: 1

   pluginlibs
   /dev/plugin_inheritance
   /dev/plugin_collaboration
   requirements

Getting acquainted
==================


.. toctree::
   :maxdepth: 1

   /dev/languages
   /tutorials/layouts

   /dev/online
   /dev/site_config

   /dev/bleach
   /dev/story
   /dev/etree

   /dev/matrix/index
   /tutorials/input_mask/index
   /dev/ovfields/index

   /dev/qtclient
   /dev/myroles
   /dev/get_handle_name


Languages
=========

.. toctree::
   :maxdepth: 1

   /dev/i18n
   /specs/de_BE
   /specs/cosi5/index
   mldbc


Internationalization
====================

.. toctree::
   :maxdepth: 1

   demonames
   /dev/translate/index
   /specs/cosi5/index

Internals
=========

.. toctree::
   :maxdepth: 1

   /dev/setup

Write docs
==========

.. toctree::
   :maxdepth: 1

   show

Other
=====

.. toctree::
   :maxdepth: 1

   /tested/index
   /dev/design
   /dev/diamond
   /dev/analysis
   /dev/django
   /dev/tours
   /dev/socialauth/index
   /dev/application
   /dev/combo/index
   /dev/combo/choosers
   /dev/projects

   /specs/uitests

   ui
   deploy
   mama
   stories
   userdocs
   dpy
   interapp
   names
   settings
   svn
   users
   gpdn
   south
   vim
   /django/index
   /dev/internals
   xml
   site_data
   local_files
   qooxdoo
   truncate
   get_change_body
   commondata

   /dev/style
   /dev/extjs
   /dev/repair


Simulate a production site
==========================

.. toctree::
   :maxdepth: 1

   /team/nginx
   /contrib/https
   logging

Thanks to
=========

.. toctree::

    /about/thanks

Newbies corner
==============

.. toctree::

    /dev/newbies/learning
    /dev/newbies/py2lino
    /dev/newbies/kids

Related projects
================

.. toctree::
   :maxdepth: 1

   /specs/getlino/index
   /specs/synodal/index
