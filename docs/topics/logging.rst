.. doctest docs/topics/logging.rst
.. _dg.topics.logging:

=================================
About logging
=================================

This document explains additional information about logging for developers.  We
assume that you have read :ref:`About logging in the Hoster's Guide
<host.logging>`.


.. contents::
    :depth: 1
    :local:

Overview
========

Lino's default logging messages go only to the console and (when an error
occurs) to the :setting:`ADMIN_EMAILS`.

When you create a :xfile:`log` directory in your project directory, Lino will
additionally log to a file :xfile:`lino.log` in that directory.

On a :term:`production site` you probably want to also run a :term:`log server`.


.. _dev.logging:

About logging in a development environment
==========================================

On my development machine I have a `runserver` script that does::

    set LINO_LOGLEVEL=DEBUG
    python manage.py runserver


Logging SQL statements
======================

The default logging level for the `django.db.backends
<https://docs.djangoproject.com/en/5.0/topics/logging/#django-db-backends>`__
handler is WARNING. You can change this default value by setting the
:envvar:`LINO_SQL_LOGLEVEL` variable.

.. envvar:: LINO_SQL_LOGLEVEL

  The logging level to set for the `django.db.backends
  <https://docs.djangoproject.com/en/5.0/topics/logging/#django-db-backends>`__
  handler instead of the default value ``WARNING``.


The Lino logger
===============

.. include:: /../docs/shared/include/tested.rst

>>> from lino import startup
>>> startup('lino_book.projects.noi1e.settings.demo')
>>> from lino.api.doctest import *

>>> from lino import logger
>>> logger.handlers
[<StreamHandler (INFO)>, <AdminEmailHandler (ERROR)>]
>>> logger.info("Hello, world!")
Hello, world!
>>> logger.debug("Foolish humans trying to understand me")


.. _history_aware_logging:

History-aware logging
=====================

When Lino is in **history-aware logging** mode, it logs a message
:message:`Started %s (using %s) --> PID %s` at process startup (and a message
:message:`Done PID %s` at termination).

This kind of messages are usually disturbing for development and testing, but
they are interesting for the :term:`system administrator` of a :term:`production
site`.

The *history-aware logging* mode is automatically set to `True` when a logger
directory (:xfile:`log`) exists in the project directory.


Testing the ``log`` directory
=============================

When a socket file exists (a file named :xfile:`lino.log.sock` in the
:xfile:`log` directory), then Lino assumes that you have a :term:`log server`
running that is responsible for writing to the :xfile:`lino.log` file.

We are going to play in the `min1` demo project:

>>> from atelier.sheller import Sheller
>>> shell = Sheller('lino_book/projects/min1')

.. cleanup from previous runs:
  >>> shell("rm -rf log")
  <BLANKLINE>

The demo sites have no :xfile:`log` directory and hence no :xfile:`lino.log`
file.

>>> shell("ls log")
ls: cannot access 'log': No such file or directory

>>> shell("python manage.py prep --noinput")
... #doctest: +ELLIPSIS +REPORT_UDIFF +NORMALIZE_WHITESPACE
`initdb std demo demo2` started on database .../min1/default.db.
...
Installed 200 object(s) from 8 fixture(s)

For the following snippets we temporarily enable file logging in the min1 demo
site by creating a :xfile:`log` directory.

>>> shell("mkdir log")
<BLANKLINE>

>>> shell("python manage.py prep --noinput")
... #doctest: +ELLIPSIS +REPORT_UDIFF +NORMALIZE_WHITESPACE
Started manage.py prep --noinput (using lino_book.projects.min1.settings) --> PID ...
`initdb std demo demo2` started on database .../min1/default.db.
...
Done manage.py prep --noinput (PID ...)


>>> shell("ls log")
lino.log

>>> shell("cat log/lino.log")
... #doctest: +ELLIPSIS +REPORT_UDIFF +NORMALIZE_WHITESPACE
2... INFO [lino ...] : Started manage.py prep --noinput (using lino_book.projects.min1.settings) --> PID ...
2... INFO [lino ...] : `initdb std demo demo2` started on database .../min1/default.db.
2...
2... INFO [lino ...] : Done manage.py prep --noinput (PID ...)


When there is a :xfile:`log` directory, the Lino logger will log to the
:xfile:`lino.log` file as well.

The following snippet demonstrates an edge case: we *simulate* a :term:`log
server` by creating a fake socket file.

>>> shell("rm log/lino.log")
<BLANKLINE>
>>> shell("touch log/lino.log.sock")
<BLANKLINE>

The process then sends log records to that socket, without asking whether the
server actually runs:

>>> shell("python manage.py prep --noinput")
... #doctest: +ELLIPSIS +REPORT_UDIFF +NORMALIZE_WHITESPACE
Started manage.py prep --noinput (using lino_book.projects.min1.settings) --> PID ...
...

But since in our case no log server is actually running, the :xfile:`lino.log`
file has not been written (only the socket file is there):

>>> shell("ls log")
lino.log.sock

Tidy up and remove all traces:

>>> shell("rm log/lino.log.sock")
<BLANKLINE>
>>> shell("rmdir log")
<BLANKLINE>



Relation between *logging level* and *verbosity*
================================================

The relation between *logging level* and *verbosity* is not yet clear.

You can set :envvar:`LINO_LOGLEVEL` to "WARNING" in order to get rid of quite
some messages:

>>> import os
>>> env = dict()
>>> env.update(os.environ)
>>> env.update(LINO_LOGLEVEL="WARNING")
>>> shell("python manage.py prep --noinput", env=env)
... #doctest: +ELLIPSIS +REPORT_UDIFF +NORMALIZE_WHITESPACE
No changes detected
Operations to perform:
  Synchronize unmigrated apps: about, bootstrap3, contacts, countries, extjs, jinja, lino, office, printing, staticfiles, system, users, xl
  Apply all migrations: sessions
Synchronizing apps without migrations:
  Creating tables...
    Creating table system_siteconfig
    Creating table users_user
    Creating table users_authority
    Creating table countries_country
    Creating table countries_place
    Creating table contacts_partner
    Creating table contacts_person
    Creating table contacts_companytype
    Creating table contacts_company
    Creating table contacts_roletype
    Creating table contacts_role
    Running deferred SQL...
Running migrations:
  Applying sessions.0001_initial... OK
Installed 200 object(s) from 8 fixture(s)

Setting :envvar:`LINO_LOGLEVEL` to "WARNING" does not remove messages issued by
Django because Django does not use the logging system to print these messages.
To get rid of these messages as well, you can set verbosity to 0:

>>> shell("python manage.py prep --noinput -v0", env=env)
... #doctest: +ELLIPSIS +REPORT_UDIFF +NORMALIZE_WHITESPACE



Site attributes related to logging
==================================

.. class:: lino.core.site.Site
  :noindex:

  .. attribute:: log_each_action_request

    Whether Lino should log every incoming request for non :attr:`readonly
    <lino.core.actions.Action.readonly>` actions.

    This is experimental. Theoretically it is useless to ask Lino for logging
    every request since the web server does this. OTOH Lino can produce more
    readable logs.

    There is no warranty that actually *each* request is being logged.  It
    currently works only for requests that are being processed by the kernel's
    :meth:`run_action <lino.core.kernel.Kernel.run_action>` methods.

  .. attribute:: logger_filename

    The name of Lino's main log file, created in :meth:`setup_logging`.

    Default value is :xfile:`lino.log`.

  .. attribute:: logger_format

    The format template to use for logging to the :xfile:`lino.log` file.

  .. attribute:: auto_configure_logger_names = 'atelier lino'

    A string with a space-separated list of logger names to be
    automatically configured. See :meth:`setup_logging`.

  .. attribute:: log_sock_path

    The full Path of the logger socket file (if this process is logging to the
    :term:`log server`, otherwise `None`).

  .. method:: setup_logging(self)

      This is called during :term:`Site startup`
      *before* any plugins are loaded because all this must
      happen *before* Django passes the setting to the
      `logging.config.dictConfig
      <https://docs.python.org/3/library/logging.config.html#logging.config.dictConfig>`__
      function.

      It does nothing if :attr:`auto_configure_logger_names` is empty.

      It modifies the :data:`DEFAULT_LOGGING
      <django.utils.log.DEFAULT_LOGGING>` setting.

      It is designed to work with the :setting:`LOGGING` and
      :setting:`LOGGING_CONFIG` settings unmodified.

      It does the following modifications:

      - Define a *default logger configuration* that is initially the same as
        the one used by Django::

          {
              'handlers': ['console', 'mail_admins'],
              'level': 'INFO',
          }

      - If the :attr:`site_dir` has a subdirectory named ``log``,
        and if :attr:`logger_filename` is not empty, add a handler
        named ``file`` and a formatter named ``verbose``, and add
        that handler to the default logger configuration.

      - Apply the default logger configuration to every logger name
        in :attr:`auto_configure_logger_names`.

      - (does no longer) configure the console handler to write to stdout
        instead of Django's default stderr (as explained `here
        <https://codeinthehole.com/writing/console-logging-to-stdout-in-django/>`__)
        because that breaks testing.

      It does nothing at all if :attr:`auto_configure_logger_names`
      is set to `None` or empty.
