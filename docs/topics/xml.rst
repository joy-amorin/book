=====================
XML reports reference
=====================

- :meth:`lino_xl.lib.finan.PaymentOrder.write_xml`
- :meth:`lino_xl.lib.bevat.Declaration.write_intracom_statement`
