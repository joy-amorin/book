=========
Old stuff
=========

.. toctree::
   :hidden:

   /examples/polly/index
   /examples/belref/index
   /dev/framework
   /install/index
   /dev/fields
   /dev/xl
   /tutorials/index
   /dev/interview
   /dev/mixins
   /team/setup
   /team/tools
   /dev/pull
   /team/deploy_prod
   /dev/oui5
   /davlink/index
   /eidreader/index
   /java/index
   /dev/newbies/prerequisites
   /dev/newbies/index
   /specs/index
   /specs/projects/index
   /specs/modlib
   /topics/cpas
   /team/install/index


.. _main_menu:

The main menu
=============

See :term:`main menu`.


.. _dashboard:

Dashboard
=========

See :term:`dashboard`.
