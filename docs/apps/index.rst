=======================
Privileged applications
=======================

Some applications have the privilege of being part of the Lino book, so their
developer documentation is provided and maintained by the Lino core team.

.. toctree::
   :maxdepth: 1


   /specs/noi/index
   /specs/cosi/index
   /specs/avanti/index
   /specs/tera/index
   /specs/polly
   /specs/care
   /specs/voga/index
   cms/index
