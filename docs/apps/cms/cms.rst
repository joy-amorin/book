.. doctest docs/apps/cms/cms.rst
.. _cms.plugins.cms:

======================================
``cms`` (main plugin for Lino CMS)
======================================

In Lino CMS this plugin defines the :xfile:`locale` directory for all
translations.

.. contents::
  :local:

.. include:: /../docs/shared/include/tested.rst

>>> import lino
>>> lino.startup('lino_book.projects.cms1.settings')
>>> from lino.api.doctest import *

A test failure said that in cms1 there are 0 (on GL) instead of 6 checkdata
errors (on my machine). That's why we'd like to see these messages:

>>> rt.show(checkdata.Messages)
... #doctest: +ELLIPSIS +NORMALIZE_WHITESPACE +REPORT_UDIFF
No data to display


>>> obj = blogs.Entry.objects.get(id=2)
>>> obj.body_short_preview
... #doctest: +ELLIPSIS +NORMALIZE_WHITESPACE +REPORT_UDIFF
'<a href="/admin/#/api/uploads/Uploads/15" target="_blank"></a><img
src="/media/uploads/2022/09/crossroads.jpg"
style="padding:4px;float:left;height:8em" title="Crossroads, Kilham West Field -
geograph.org.uk - 2097672"/>Let\'s choose one or the other of the either roads
(or NOT)!\n\nAnd the hesitation, does it comes rarely(?), Nooo!, we are very
frequently and suddenly put to situations where we must choose between
roads.\n\nOf course, how to choose and what to choose are the questions. But did
we ever ask \'why?\' But of c...'
