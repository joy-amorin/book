.. doctest docs/plugins/users.rst
.. _cms.plugins.users:

==================================
``users`` in Lino CMS
==================================

The :mod:`lino_cms.lib.users` plugin extends :mod:`lino.modlib.users`.

.. contents::
  :local:

.. include:: /../docs/shared/include/tested.rst

>>> import lino
>>> lino.startup('lino_book.projects.cms1.settings')
>>> from lino.api.doctest import *


Available user types
====================

Lino CMS knows the following :term:`user types <user type>`:

>>> rt.show(rt.models.users.UserTypes)
======= =========== ===============
 value   name        text
------- ----------- ---------------
 000     anonymous   Anonymous
 100     user        User
 800     staff       Staff
 900     admin       Administrator
======= =========== ===============
<BLANKLINE>

A :term:`demo site` has the following users:

>>> rt.show(rt.models.users.UsersOverview)
... #doctest: +NORMALIZE_WHITESPACE +REPORT_UDIFF
========== ===================== ==========
 Username   User type             Language
---------- --------------------- ----------
 robin      900 (Administrator)   en
 rolf       900 (Administrator)   de
 romain     900 (Administrator)   fr
========== ===================== ==========
<BLANKLINE>


The site manager
================

Robin is a :term:`site manager`, he has a complete menu.

>>> show_menu('robin')
... #doctest: +ELLIPSIS +NORMALIZE_WHITESPACE +REPORT_UDIFF
- Publisher : Pages, My Blog entries, Sources
- Office : My Comments, Recent comments, Data problem messages assigned to me, My Upload files
- Configure :
  - Publisher : Special pages, Blog Entry Types, Topics, Albums, Licenses, Authors
  - System : Users, Site contacts, Site Parameters, System tasks
  - Office : Comment Types, Library volumes, Upload types
- Explorer :
  - System : Authorities, User types, User roles, Third-party authorizations, Data checkers, Data problem messages, Background procedures, content types
  - Publisher : Blog entries, Tags
  - Office : Comments, Reactions, Upload files, Upload areas, Mentions
- Site : About, User sessions
