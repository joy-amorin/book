.. _api:

===
API
===

We are migrating documentation of the core Lino modules from docstrings (in
:mod:`lino.core`) to prosa format (in this section).

.. toctree::
    :maxdepth: 1

    ad/index
    dd/index
    rt/index

The ``lino.api`` package contains a series of modules that encapsulate Lino's
core functionalities.  They don't define anything on their own but just import
things that are commonly used in different contexts. One module for each of the
three startup phases used when writing application code:

- :doc:`ad/index` contains classes and functions that are available
  already **before** your Lino application gets initialized.  You use
  it to define your **overall application structure** (in your
  :xfile:`settings.py` files and in the :xfile:`__init__.py` files of
  your plugins).

- :doc:`dd/index` is for when you are **describing your database schema** in
  your :xfile:`models.py` modules.

- :doc:`rt/index` contains functions and classes that are commonly used "at
  runtime", i.e. when the Lino application has been initialized. You may
  *import* it at the global namespace of a :xfile:`models.py` file, but you can
  *use* most of it only when the :func:`startup` function has been called.

Recommended usage is to import these modules as follows::

  from lino.api import ad, dd, rt, _


The remaining part of this document has been automatically generated from the
source code.

.. contents::
    :depth: 2
    :local:


The ``lino`` package
====================

.. automodule:: lino


The ``lino_xl`` package
=======================

.. automodule:: lino_xl


The ``lino_react`` package
==========================

.. automodule:: lino_react


The ``lino_book`` package
=========================

.. py2rst::

  import lino_book
  print(lino_book.SETUP_INFO['long_description'])

.. automodule:: lino_book


.. _lino.apps:

Lino applications covered by the book
=====================================

The following Lino applications have their developer specs and API in the Lino
book and form an integral part of the Lino core because they also serve for
testing purposes.


Lino Noi
--------

.. automodule:: lino_noi


Lino Così
---------

.. automodule:: lino_cosi


Lino Avanti
-----------

.. automodule:: lino_avanti

Lino Vilma
----------

.. automodule:: lino_vilma

Lino Care
---------

.. automodule:: lino_care

Lino Tera
---------

.. automodule:: lino_tera

Lino Voga
---------

.. automodule:: lino_voga

Lino CMS
---------

.. automodule:: lino_cms
