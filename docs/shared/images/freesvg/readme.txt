The svg images in this directory are public domain. We collected them from
the following sources:

- https://freesvg.org
- https://publicdomainvectors.org

We use them via substitutions defined in /docs/include/freesvg.rst
