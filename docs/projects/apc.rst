.. doctest docs/projects/apc.rst
.. _cosi.tested.demo:
.. _specs.cosi.apc:

==================================================
``apc`` : A Lino Così for Belgium (DE)
==================================================

.. include:: /../docs/shared/include/tested.rst

>>> from lino import startup
>>> startup('lino_book.projects.apc.settings.doctests')
>>> from lino.api.doctest import *
>>> ses = rt.login('robin')

Overview
========

>>> print(analyzer.show_complexity_factors())
... #doctest: +ELLIPSIS +NORMALIZE_WHITESPACE +REPORT_UDIFF
- 36 plugins
- 60 models
- 3 user types
- 203 views
- 18 dialog actions
<BLANKLINE>


>>> rt.show(accounting.JournalsOverview)
**72 Verkaufsrechnungen (SLS)**
**0 Gutschriften Verkauf (SLC)**
**105 Einkaufsrechnungen (PRC)**
**14 Zahlungsaufträge (PMO)**
**0 Kassenbuch (CSH)**
**14 Bestbank (BNK)**
**0 Diverse Buchungen (MSC)**
**1 Preliminary transactions (PRE)**
**0 Lohnscheine (SAL)**
**14 MwSt.-Erklärungen (VAT)**

>>> rt.show(invoicing.Tasks)
... #doctest: +NORMALIZE_WHITESPACE +REPORT_UDIFF
===== ============= ========================== ===================== =============== ========== =========== =======================
 Nr.   Autor         Zieljournal                Invoice generators    Logging level   Disabled   Wann        Status
----- ------------- -------------------------- --------------------- --------------- ---------- ----------- -----------------------
 1     Rolf Rompen   Verkaufsrechnungen (SLS)   trading.InvoiceItem   INFO            Nein       Jeden Tag   Scheduled to run asap
===== ============= ========================== ===================== =============== ========== =========== =======================
<BLANKLINE>


Implementation details
======================

>>> print(settings.SETTINGS_MODULE)
lino_book.projects.apc.settings.doctests

>>> print(' '.join([lng.name for lng in settings.SITE.languages]))
de fr en

The demo database contains 69 persons and 23 companies.

>>> contacts.Person.objects.count()
69
>>> contacts.Company.objects.count()
24
>>> contacts.Partner.objects.count()
93


>>> print(' '.join(settings.SITE.demo_fixtures))
std few_countries minimal_ledger furniture demo demo_bookings payments demo2 demo3 checkdata



The application menu
====================

Robin is the system administrator, he has a complete menu:

>>> show_menu('rolf')
... #doctest: +ELLIPSIS +NORMALIZE_WHITESPACE +REPORT_UDIFF
- Kontakte : Personen, Organisationen
- Büro : Meine Auszüge, Meine Upload-Dateien
- Verkauf : Mein Fakturationsplan, Verkaufsrechnungen (SLS), Gutschriften Verkauf (SLC)
- Buchhaltung :
  - Einkauf : Einkaufsrechnungen (PRC)
  - Löhne und Gehälter : Lohnscheine (SAL)
  - Finanzjournale : Zahlungsaufträge (PMO), Kassenbuch (CSH), Bestbank (BNK)
  - MwSt. : MwSt.-Erklärungen (VAT)
  - Diverse Buchungen : Diverse Buchungen (MSC), Preliminary transactions (PRE)
- Berichte :
  - Verkauf : Offene Rechnungen
  - Buchhaltung : Buchhaltungsbericht, Schuldner, Gläubiger
  - MwSt. : Intra-Community purchases, Intra-Community sales
- Konfigurierung :
  - System : Benutzer, Site-Parameter, System tasks
  - Orte : Länder, Orte
  - Kontakte : Rechtsformen, Funktionen
  - Büro : Auszugsarten, Dateibibliotheken, Upload-Arten, Meine Einfügetexte
  - Verkauf : Produkte, Produktkategorien, Preisregeln, Papierarten, Pauschalen, Folgeregeln, Fakturierungsaufgaben
  - Buchhaltung : Jahresberichtspositionen, Konten, Journale, Geschäftsjahre, Buchungsperioden, Zahlungsbedingungen
- Explorer :
  - System : Vollmachten, Benutzerarten, Benutzerrollen, Datenbankmodelle, Background procedures, Datentests, Datenproblemmeldungen
  - Kontakte : Kontaktpersonen, Partner, Kontaktangabenarten, Kontaktangaben
  - Büro : Auszüge, Upload-Dateien, Upload-Bereiche, Einfügetexte, Erwähnungen
  - SEPA : Bankkonten
  - Verkauf : Price factors, Verkaufsrechnungen, Sales invoice items, Fakturationspläne, Verkaufsregeln
  - Finanzjournale : Kontoauszüge, Diverse Buchungen, Zahlungsaufträge
  - Buchhaltung : Accounting Reports, Allgemeine Jahresberichtspositionen, General account balances, Analytic accounts balances, Partner balances, Sheet item entries, Gemeinkonten, Begleichungsregeln, Belege, Belegarten, Bewegungen, Handelsarten, Journalgruppen
  - MwSt. : Belgische MwSt.-Erklärungen, Declaration fields, MWSt-Zonen, MwSt.-Regimes, MwSt.-Klassen, MWSt-Kolonnen, Rechnungen, MwSt-Regeln
- Site : Info, Benutzersitzungen


Database structure
==================

>>> from lino.utils.diag import analyzer
>>> print analyzer.show_database_structure()
... #doctest: +NORMALIZE_WHITESPACE +REPORT_UDIFF +ELLIPSIS +SKIP


.. _internal_clearings:

Internal clearings
==================

An **internal clearing** is when an employee acts as a temporary cashier by
paying purchase invoices or taking money for sales invoices.

When a site has a non-empty :attr:`worker_model
<lino_xl.lib.accounting.Plugin.worker_model>`,  Lino adds a field :attr:`worker
<lino_xl.lib.accounting.PaymentTerm.worker>` to each payment term.

When an invoice is registered with a payment term having a :attr:`worker
<lino_xl.lib.accounting.PaymentTerm.worker>`, Lino will book two additional
movements: one which cleans the debit (credit) on the customer (provider) by
booking back the total amount, and a second to book the invoiced amount as a
debit or credit on the worker (using the :attr:`main_account
<lino_xl.lib.accounting.TradeType.main_account>` for :attr:`TradeTypes.wages
<lino_xl.lib.accounting.TradeTypes.wages>`).


>>> rt.show(accounting.PaymentTerms, language="en", column_names="ref name_en months days worker")
==================== ======================================= ======== ========= =================
 Reference            Designation (en)                        Months   Days      Worker
-------------------- --------------------------------------- -------- --------- -----------------
 07                   Payment seven days after invoice date   0        7
 10                   Payment ten days after invoice date     0        10
 30                   Payment 30 days after invoice date      0        30
 60                   Payment 60 days after invoice date      0        60
 90                   Payment 90 days after invoice date      0        90
 EOM                  Payment end of month                    0        0
 P30                  Prepayment 30%                          0        30
 PIA                  Payment in advance                      0        0
 robin                Cash Robin                              0        0         Mr Robin Dubois
 **Total (9 rows)**                                           **0**    **227**
==================== ======================================= ======== ========= =================
<BLANKLINE>

>>> dd.plugins.accounting.worker_model
<class 'lino_xl.lib.contacts.models.Person'>

And as we can see, our worker Robin owes us 9784,48 € because he took money for
7 sales invoices:

>>> robin = dd.plugins.accounting.worker_model.objects.get(first_name="Robin")
>>> rt.show(accounting.MovementsByPartner, master_instance=robin)
**7 offene Bewegungen (-8737.10 €)**

>>> rt.show(accounting.MovementsByPartner, master_instance=robin, nosummary=True)
========== ===================== ======================================================================================== ============== ======== ============= ===========
 Valuta     Beleg                 Beschreibung                                                                             Debit          Kredit   Match         Beglichen
---------- --------------------- ---------------------------------------------------------------------------------------- -------------- -------- ------------- -----------
 08.03.15   `SLS 11/2015 <…>`__   `(4800) Internal clearings <…>`__ | `Radermacher Inge <…>`__ | `Dubois Robin <…>`__      2 468,18                SLS 11/2015   Nein
 07.01.15   `SLS 1/2015 <…>`__    `(4800) Internal clearings <…>`__ | `Radermacher Alfons <…>`__ | `Dubois Robin <…>`__    38,62                   SLS 1/2015    Nein
 07.11.14   `SLS 49/2014 <…>`__   `(4800) Internal clearings <…>`__ | `Lazarus Line <…>`__ | `Dubois Robin <…>`__          453,75                  SLS 49/2014   Nein
 10.09.14   `SLS 38/2014 <…>`__   `(4800) Internal clearings <…>`__ | `Ingels Irene <…>`__ | `Dubois Robin <…>`__          726,00                  SLS 38/2014   Nein
 11.06.14   `SLS 29/2014 <…>`__   `(4800) Internal clearings <…>`__ | `Evertz Bernd <…>`__ | `Dubois Robin <…>`__          2 782,77                SLS 29/2014   Nein
 07.05.14   `SLS 19/2014 <…>`__   `(4800) Internal clearings <…>`__ | `Bastiaensen Laurent <…>`__ | `Dubois Robin <…>`__   1 451,82                SLS 19/2014   Nein
 10.02.14   `SLS 9/2014 <…>`__    `(4800) Internal clearings <…>`__ | `Hans Flott & Co <…>`__ | `Dubois Robin <…>`__       815,96                  SLS 9/2014    Nein
                                  **Saldo 8737.10 (7 Bewegungen)**                                                         **8 737,10**
========== ===================== ======================================================================================== ============== ======== ============= ===========
<BLANKLINE>


Miscellaneous
=============

Person #115 is not a Partner
----------------------------

Person #115 (u'Altenberg Hans') is not a Partner (master_key
is <django.db.models.fields.related.ForeignKey: partner>)

>>> url = '/bs3/contacts/Person/115'
>>> test_client.force_login(rt.login('robin').user)
>>> res = test_client.get(url, REMOTE_USER='robin')
>>> print(res.status_code)
200


Slave tables with more than 15 rows
-----------------------------------

When you look at the detail window of Belgium in `Lino Così
<http://demo4.lino-framework.org/api/countries/Countries/BE?an=detail>`_
then you see a list of all places in Belgium.
This demo database contains exactly 48 entries:

>>> be = countries.Country.objects.get(isocode="BE")
>>> be.place_set.count()
48

>>> countries.PlacesByCountry.request(be).get_total_count()
48

>>> url = '/api/countries/PlacesByCountry?fmt=json&start=0&mt=10&mk=BE'
>>> res = test_client.get(url,REMOTE_USER='robin')
>>> print(res.status_code)
200
>>> result = json.loads(res.content.decode('utf-8'))
>>> print(len(result['rows']))
15

The 15 is because Lino has a hard-coded default value of
returning only 15 rows when no limit has been specified.

In versions after :blogref:`20130903` you can change that limit
for a given table by overriding the
:attr:`preview_limit <lino.core.tables.AbstractTable.preview_limit>`
parameter of your table definition.
Or you can change it globally for all your tables
by setting the
:attr:`preview_limit <ad.Site.preview_limit>`
Site attribute to either `None` or some bigger value.

This parameter existed before but wasn't tested.
In your code this would simply look like this::

  class PlacesByCountry(Places):
      preview_limit = 30

Here we override it on the living object:

>>> countries.PlacesByCountry.preview_limit = 25

Same request returns now 25 data rows:

>>> res = test_client.get(url, REMOTE_USER='robin')
>>> result = json.loads(res.content.decode('utf-8'))
>>> print(len(result['rows']))
25

To remove the limit altogether, you can say:

>>> countries.PlacesByCountry.preview_limit = None

and the same request now returns all 49 data rows:

>>> res = test_client.get(url,REMOTE_USER='robin')
>>> result = json.loads(res.content.decode('utf-8'))
>>> print(len(result['rows']))
49

This site shows a series of due sales invoices
(:class:`lino_xl.lib.trading.DueInvoices`).

>>> rt.show(trading.DueInvoices)
... #doctest: +ELLIPSIS +NORMALIZE_WHITESPACE +REPORT_UDIFF
===================== =========== ========= ===================== =============== ================ ================
 Due date              Reference   No.       Partner               Total to pay    Balance before   Balance to pay
--------------------- ----------- --------- --------------------- --------------- ---------------- ----------------
 10/02/2014            SLS         9         Hans Flott & Co       815,96
 07/05/2014            SLS         19        Bastiaensen Laurent   1 451,82
 11/06/2014            SLS         29        Evertz Bernd          2 782,77
 10/09/2014            SLS         38        Ingels Irene          726,00
 07/11/2014            SLS         49        Lazarus Line          453,75
 07/01/2015            SLS         1         Radermacher Alfons    38,62
 08/03/2015            SLS         11        Radermacher Inge      2 468,18
 09/03/2015            SLS         12        Radermacher Jean      822,57                           822,57
 12/03/2015            SLS         5         Radermacher Edgard    3 387,78                         3 387,78
 17/03/2015            SLS         13        di Rupo Didier        338,80                           338,80
 21/03/2015            SLS         14        da Vinci David        647,35                           647,35
 22/03/2015            SLS         15        da Vinci David        1 314,03        647,35           1 314,03
 06/04/2015            SLS         10        Radermacher Hedi      3 629,82                         3 629,82
 14/05/2015            SLS         8         Radermacher Guido     895,40                           268,62
 **Total (14 rows)**               **233**                         **19 772,85**   **647,35**       **10 408,97**
===================== =========== ========= ===================== =============== ================ ================
<BLANKLINE>


>>> show_choicelists()
... #doctest: +ELLIPSIS +NORMALIZE_WHITESPACE +REPORT_UDIFF
=========================== ======== ================= ===================================== ===================================== ============================
 name                        #items   preferred_width   de                                    fr                                    en
--------------------------- -------- ----------------- ------------------------------------- ------------------------------------- ----------------------------
 about.DateFormats           4        8                 Date formats                          Date formats                          Date formats
 about.TimeZones             1        4                 Zeitzonen                             Zeitzonen                             Time zones
 accounting.CommonAccounts   21       29                Gemeinkonten                          Comptes communs                       Common accounts
 accounting.DC               2        6                 Buchungsrichtungen                    Buchungsrichtungen                    Booking directions
 accounting.JournalGroups    6        18                Journalgruppen                        Groupes de journaux                   Journal groups
 accounting.PeriodStates     2        14                Zustände                              États                                 States
 accounting.TradeTypes       6        18                Handelsarten                          Types de commerce                     Trade types
 accounting.VoucherStates    4        14                Belegzustände                         Belegzustände                         Voucher states
 accounting.VoucherTypes     6        55                Belegarten                            Types de pièce                        Voucher types
 bevat.DeclarationFields     29       4                 Declaration fields                    Declaration fields                    Declaration fields
 checkdata.Checkers          13       90                Datentests                            Tests de données                      Data checkers
 contacts.CivilStates        7        27                Zivilstände                           Etats civils                          Civil states
 contacts.PartnerEvents      1        18                Beobachtungskriterien                 Évènements observés                   Observed events
 countries.PlaceTypes        23       16                None                                  None                                  None
 excerpts.Shortcuts          0        4                 Excerpt shortcuts                     Excerpt shortcuts                     Excerpt shortcuts
 invoicing.Periodicities     3        15                Abonnementperiodizitäten              Abonnementperiodizitäten              Subscription periodicities
 linod.LogLevels             5        8                 Logging levels                        Logging levels                        Logging levels
 linod.Procedures            2        19                Background procedures                 Background procedures                 Background procedures
 phones.ContactDetailTypes   6        8                 Kontaktangabenarten                   Kontaktangabenarten                   Contact detail types
 printing.BuildMethods       6        20                None                                  None                                  None
 products.BarcodeDrivers     2        4                 Barcode drivers                       Barcode drivers                       Barcode drivers
 products.DeliveryUnits      4        7                 Liefereinheiten                       Unités de livraison                   Delivery units
 products.PriceFactors       0        4                 Price factors                         Price factors                         Price factors
 products.ProductTypes       1        8                 Product types                         Product types                         Product types
 sheets.CommonItems          29       49                Allgemeine Jahresberichtspositionen   Allgemeine Jahresberichtspositionen   Common sheet items
 sheets.SheetTypes           2        23                Jahresbericht-Arten                   Jahresbericht-Arten                   Sheet types
 system.DashboardLayouts     1        17                None                                  None                                  None
 system.DurationUnits        7        8                 None                                  None                                  None
 system.Genders              3        10                Genders                               Genders                               Genders
 system.PeriodEvents         3        9                 Beobachtungskriterien                 Évènements observés                   Observed events
 system.Recurrences          11       20                Wiederholungen                        Récurrences                           Recurrences
 system.Weekdays             7        10                None                                  None                                  None
 system.YesNo                2        12                Ja oder Nein                          Oui ou non                            Yes or no
 uploads.Shortcuts           0        4                 Upload shortcuts                      Upload shortcuts                      Upload shortcuts
 uploads.UploadAreas         1        7                 Upload-Bereiche                       Domaines de téléchargement            Upload areas
 users.UserTypes             3        15                Benutzerarten                         Types d'utilisateur                   User types
 vat.DeclarationFieldsBase   0        4                 Declaration fields                    Declaration fields                    Declaration fields
 vat.VatAreas                3        13                MWSt-Zonen                            Zones TVA                             VAT areas
 vat.VatClasses              6        31                MwSt.-Klassen                         Classes TVA                           VAT classes
 vat.VatColumns              10       34                MWSt-Kolonnen                         MWSt-Kolonnen                         VAT columns
 vat.VatRegimes              11       24                MwSt.-Regimes                         MwSt.-Regimes                         VAT regimes
 vat.VatRules                27       185               MwSt-Regeln                           MwSt-Regeln                           VAT rules
 xl.Priorities               5        8                 Prioritäten                           Priorités                             Priorities
=========================== ======== ================= ===================================== ===================================== ============================
<BLANKLINE>


Verify whether :ticket:`3657` is fixed:

>>> print(rt.find_config_file("logo.jpg", "weasyprint"))  #doctest: +ELLIPSIS
/.../lino_book/projects/apc/settings/config/weasyprint/logo.jpg

Verify whether :ticket:`3705` is fixed:

>>> for cd in settings.SITE.confdirs.config_dirs:
...     print(cd.name)  #doctest: +ELLIPSIS
/.../lino_book/projects/apc/settings/config
/.../lino_xl/lib/sheets/config
...
/.../lino/modlib/jinja/config
/.../lino/config
