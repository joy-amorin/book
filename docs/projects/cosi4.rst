.. doctest docs/projects/cosi4.rst
.. _book.projects.cosi4:

=========================================
``cosi4`` : a Lino Così for Uruguay
=========================================

.. module:: lino_book.projects.cosi4

A :term:`demo project` showing a :ref:`cosi` configured for usage in Uruguay.


>>> from lino import startup
>>> startup('lino_book.projects.cosi4.settings')
>>> from lino.api.doctest import *


Overview
========

The :mod:`lino_book.projects.cosi4` demo project is an example of a :ref:`cosi`
having

- :attr:`lino_xl.lib.contacts.Plugin.demo_region` set to ``UY``

  >>> dd.plugins.contacts.demo_region
  'UY'

- Spanish as second language

  >>> [i.django_code for i in settings.SITE.languages]
  ['en', 'es']

It also has :setting:`countries.full_data` set to True and because of this is
used by :doc:`/ref/demo_fixtures`.

The :mod:`lino.utils.demonames.ury` module  already localizes names of poeple,
but names of places and streets are not yet implemented.

>>> rt.show('contacts.Persons')
... #doctest: +ELLIPSIS +REPORT_UDIFF
======================= ========================================== =================== ============== ======== ===== ==========
 Name                    Address                                    e-mail address      Phone          Mobile   ID    Language
----------------------- ------------------------------------------ ------------------- -------------- -------- ----- ----------
 Mr Ademir Abreu         Akazienweg, 4700 Eupen, Belgium            andreas@arens.com   +32 87123456            112
 Mrs Agustina Acevedo    Alter Malmedyer Weg, 4700 Eupen, Belgium   annette@arens.com   +32 87123457            113
 Mr Aldemir Acosta       Aachener Straße, 4700 Eupen, Belgium                                                   114
 Mr Aldrin Acua          Am Bahndamm, 4700 Eupen, Belgium                                                       115
 Mr Ale Aguiar           Am Berg, 4700 Eupen, Belgium                                                           116
 Mrs Aliki Aguilar       Auf dem Spitzberg, 4700 Eupen, Belgium                                                 117
 Mrs Alondra Aguilera    Auenweg, 4700 Eupen, Belgium                                                           118
 Mr Alekos Aguirre       Am Waisenbüschchen, 4700 Eupen, Belgium                                                119
 Mr Alexander Albornoz   August-Thonnar-Str., 4700 Eupen, Belgium                                               120
 Mrs Andrea Alfaro       Auf'm Rain, 4700 Eupen, Belgium                                                        121
 Mrs Andressa Alfonso    Bahnhofstraße, 4700 Eupen, Belgium                                                     122
 Mrs Angelina Almada     Bahnhofsgasse, 4700 Eupen, Belgium                                                     123
 Mrs Anny Almeida        Bergkapellstraße, 4700 Eupen, Belgium                                                  124
 Mr Asaiah Alonso        Binsterweg, 4700 Eupen, Belgium                                                        125
 Mr Axel Alonzo          Bergstraße, 4700 Eupen, Belgium                                                        126
 Mr Azarel Altez         Bellmerin, 4700 Eupen, Belgium                                                         127
 Mr Babu Alvarez         Bennetsborn, 4700 Eupen, Belgium                                                       128
 Mr Bento Alves          Brabantstraße, 4700 Eupen, Belgium                                                     129
 Mrs Ayelen Alvez        Buchenweg, 4700 Eupen, Belgium                                                         130
 Mr Bruno Amaral         Edelstraße, 4700 Eupen, Belgium                                                        131
 Mrs Bettina Amaro       Favrunpark, 4700 Eupen, Belgium                                                        132
 Mr Clever Amorin        Euregiostraße, 4700 Eupen, Belgium                                                     133
 Mrs Camila Andrada      Feldstraße, 4700 Eupen, Belgium                                                        134
 Mr Darwin Andrade       Gewerbestraße, 4700 Eupen, Belgium                                                     135
 Mrs Cecilia Antunez     Fränzel, 4700 Eupen, Belgium                                                           136
 Mr Dereck Aparicio      Gospert, 4700 Eupen, Belgium                                                           137
 Mr Derian Aquino        Gülcherstraße, 4700 Eupen, Belgium                                                     138
 Mr Diaval Aranda        Haagenstraße, 4700 Eupen, Belgium                                                      139
 Mr Diego Araujo         Haasberg, 4700 Eupen, Belgium                                                          140
 Mr Dilan Arbelo         Haasstraße, 4700 Eupen, Belgium                                                        141
 Mrs Eimy Arevalo        Habsburgerweg, 4700 Eupen, Belgium                                                     142
 Mrs Eliana Arias        Heidberg, 4700 Eupen, Belgium                                                          143
 Mrs Eliane Artigas      Heidgasse, 4700 Eupen, Belgium                                                         144
 Mr Elwin Avila          Heidhöhe, 4700 Eupen, Belgium                                                          145
 Mrs Elizabeth Ayala     Herbesthaler Straße, 4700 Eupen, Belgium                                               146
 Mr Emmanuel Baez        Hochstraße, 4700 Eupen, Belgium                                                        147
 Mrs Emy Banchero        Hisselsgasse, 4700 Eupen, Belgium                                                      148
 Mr Enzo Barboza         4730 Raeren, Belgium                                                                   149
 Mr Erich Barcelo        4730 Raeren, Belgium                                                                   150
 Mrs Evelyn Barreiro     4730 Raeren, Belgium                                                                   151
 Mr Evan Barrera         4730 Raeren, Belgium                                                                   152
 Mrs Florencia Barreto   4730 Raeren, Belgium                                                                   153
 Mr Felipe Barrios       4730 Raeren, Belgium                                                                   154
 Mrs Geneviève Barros    4730 Raeren, Belgium                                                                   155
 Mr Franco Batista       4730 Raeren, Belgium                                                                   156
 Mr Frank Bello          4730 Raeren, Belgium                                                                   157
 Mr Freddy Beltran       4730 Raeren, Belgium                                                                   158
 Mr Gabriel Benitez      4730 Raeren, Belgium                                                                   159
 Mrs Grissel Bentancor   4730 Raeren, Belgium                                                                   160
 Mrs Heimy Bentancur     4730 Raeren, Belgium                                                                   161
 Mr Gaston Bentos        4730 Raeren, Belgium                                                                   162
 Mr Georgian Bermudez    4730 Raeren, Belgium                                                                   163
 Mr Geovanni Berrutti    4730 Raeren, Belgium                                                                   164
 Mr Gerardo Bianchi      4730 Raeren, Belgium                                                                   165
 Mrs Isabel Blanco       4730 Raeren, Belgium                                                                   166
 Mr Gregory Bonilla      4730 Raeren, Belgium                                                                   167
 Mrs Jessica Borba       4730 Raeren, Belgium                                                                   168
 Dr. Jimena Borges       4031 Angleur, Belgium                                                                  169
 Josefina Bravo          4031 Angleur, Belgium                                                                  170
 Mr Ignacio Britos       Amsterdam, Kingdom of the Netherlands                                                  171
 Mr Ihan Brum            Amsterdam, Kingdom of the Netherlands                                                  172
 Mrs Kamila Brun         Amsterdam, Kingdom of the Netherlands                                                  173
 Mr Illya Bruno          Aachen, Germany                                                                        174
 Mrs Kassandra Bueno     Aachen, Germany                                                                        175
 Mr Ismael Burgos        Aachen, Germany                                                                        176
 Mr Ithan Caballero      Aachen, Germany                                                                        177
 Mr Ivo Cabral           Paris, France                                                                          178
 Mr Jalen Cabrera        Paris, France                                                                          179
 Mr Javier Caceres       Paris, France                                                                          180
======================= ========================================== =================== ============== ======== ===== ==========
<BLANKLINE>
