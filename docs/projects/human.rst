.. doctest docs/projects/human.rst
.. _book.projects.human:

=================================================
``human`` : Testing some human properties
=================================================

.. module:: lino_book.projects.human

A :term:`demo project` used in
:doc:`/specs/human` and
:doc:`/specs/born`.
It is not meant to be used via the web interface.

>>> from lino import startup
>>> startup('lino_book.projects.human.settings')
>>> from lino.api.doctest import *
>>> ses = rt.login('robin')

>>> analyzer.show_db_overview()
... #doctest: +ELLIPSIS +NORMALIZE_WHITESPACE +REPORT_UDIFF
9 plugins: lino, about, jinja, bootstrap3, extjs, staticfiles, human, printing, system.
2 models:
=================== ==================== ========= =======
 Name                Default table        #fields   #rows
------------------- -------------------- --------- -------
 human.Person        human.PersonTable    7         0
 system.SiteConfig   system.SiteConfigs   3         0
=================== ==================== ========= =======
<BLANKLINE>
