.. doctest docs/projects/cms1.rst
.. _book.projects.cms1:

=======================================
``cms1`` : a content management system
=======================================

.. module:: lino_book.projects.cms1

A :term:`demo project` showing a :ref:`cms`.

It includes a page with usage examples of the :cmd:`[upload]` and
:cmd:`[gallery]` commands. To see it, say :cmd:`go cms1` followed by :cmd:`pm
runserver` and then point your browser to http://127.0.0.1:8000/p/9


>>> from lino import startup
>>> startup('lino_book.projects.cms1.settings')
>>> from lino.api.doctest import *
>>> mp = settings.SITE.plugins.memo.parser

>>> rt.models.uploads.Upload.objects.get(pk=1)
Upload #1 ('Murder on the orient express cover')


>>> print(mp.parse("[upload 1] Some text."))
... #doctest: +NORMALIZE_WHITESPACE
<a href="/admin/#/api/uploads/Uploads/1" target="_blank"/><img
src="/media/uploads/2022/09/MurderontheOrientExpress.jpg" style="padding:4px;
height:30ex" title="Murder on the orient express cover"/></a> Some text.

>>> print(mp.parse("[upload 1 My caption] Some text."))
<a href="/admin/#/api/uploads/Uploads/1" target="_blank"/><img src="/media/uploads/2022/09/MurderontheOrientExpress.jpg" style="padding:4px; height:30ex" title="My caption"/></a> Some text.

>>> print(mp.parse("[upload 1 thumb|My caption] Some text."))
... #doctest: +NORMALIZE_WHITESPACE
<a href="/admin/#/api/uploads/Uploads/1" target="_blank"/><img
src="/media/uploads/2022/09/MurderontheOrientExpress.jpg"
style="padding:4px; float:right; height:30ex" title="My caption"/></a> Some text.

>>> print(mp.parse("[upload 1 thumb|right|My caption] Some text."))
... #doctest: +NORMALIZE_WHITESPACE
<a href="/admin/#/api/uploads/Uploads/1" target="_blank"/><img
src="/media/uploads/2022/09/MurderontheOrientExpress.jpg" style="padding:4px;
float:right; height:30ex" title="My caption"/></a> Some text.

>>> print(mp.parse("[upload 1 right|thumb|My caption] Some text."))
... #doctest: +NORMALIZE_WHITESPACE
[ERROR Invalid format name 'right' (allowed names are ('thumb', 'tiny', 'wide',
'solo', 'duo', 'trio')). in '[upload 1 right|thumb|My caption]' at position
0-33] Some text.

>>> print(mp.parse("[upload 1 thumb|right|] Some text."))
<a href="/admin/#/api/uploads/Uploads/1" target="_blank"/><img src="/media/uploads/2022/09/MurderontheOrientExpress.jpg" style="padding:4px; float:right; height:30ex" title="Murder on the orient express cover"/></a> Some text.

The only difference between ``thumb`` and ``tiny`` is the size of the image. For
``thumb`` it has a height of `10em` and for ``tiny`` the height is `5em`.

We don't specify the width in order to let the browser compute it. We specify
the height and not the width because we don't care about whether the image is
landscape or portrait.

>>> print(mp.parse("[upload 1 tiny|My caption] Some text."))
... #doctest: +NORMALIZE_WHITESPACE
<a href="/admin/#/api/uploads/Uploads/1" target="_blank"/><img src="/media/uploads/2022/09/MurderontheOrientExpress.jpg" style="padding:4px; float:right; height:15ex" title="My caption"/></a> Some text.


>>> print(mp.parse("[upload 1 wide|A wide image] Some text."))
<a href="/admin/#/api/uploads/Uploads/1" target="_blank"/><img src="/media/uploads/2022/09/MurderontheOrientExpress.jpg" style="padding:4px; width:100%" title="A wide image"/></a> Some text.

Spaces around the pipe character don't count:

>>> print(mp.parse("[upload 1 wide | A wide image] Some text."))
<a href="/admin/#/api/uploads/Uploads/1" target="_blank"/><img src="/media/uploads/2022/09/MurderontheOrientExpress.jpg" style="padding:4px; width:100%" title="A wide image"/></a> Some text.


The syntax is given by the
:func:`rstgen.sphinxconf.sigal_image.parse_image_spec`
function.

Don't read me
=============

The following request had caused a traceback:

>>> res = test_client.get("/r/11")
>>> txt = beautiful_soup(res.content.decode()).text
>>> "Private collection by Luc Saffre" in txt
True

But let's  extend above test to systematically loop over all publisher locations
and GET each item:

>>> for loc, dv in dd.plugins.publisher.locations:
...     for obj in dv.model.objects.all():
...         url = "/{}/{}".format(loc, obj.pk)
...         res = test_client.get(url)
...         if res.status_code not in {200, 302}:
...             print(url, "failed with", res.status_code)
