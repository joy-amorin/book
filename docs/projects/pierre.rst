.. doctest docs/projects/pierre.rst

==================================================
``pierre`` : A Lino Così for Belgium (FR)
==================================================

.. include:: /../docs/shared/include/tested.rst

>>> from lino import startup
>>> startup('lino_book.projects.pierre.settings.demo')
>>> from lino.api.doctest import *
>>> ses = rt.login('robin')


Used in miscellaneous tested documents, e.g.

- :doc:`/dev/quantities`
- :doc:`/plugins/trading`
- :doc:`/topics/vies`
