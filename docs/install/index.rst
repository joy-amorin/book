===============
Installing Lino
===============

Content moved to
:doc:`/dev/install/index`,
:doc:`/team/install/index` and
:ref:`lino.admin.install`.
