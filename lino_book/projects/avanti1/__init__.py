# -*- coding: UTF-8 -*-
# Copyright 2017-2018 Rumma & Ko OÜ
# License: GNU Affero General Public License v3 (see file COPYING for details)
"""
Demo of :ref:`avanti`.

.. autosummary::
   :toctree:

   settings
   tests
"""
