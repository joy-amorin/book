#!/usr/bin/env bash
set -e
rm -rf migrations
rm -f default.db
echo "Removed migrations and database."
