"""This package contains example applications included with the Lino
book.  These can be used either out of the box or as a base for
derivated work.
See :doc:`/dev/projects` which explains how to try them.

Content on this page is progressively being migrated to a narrative style in
:doc:`/projects/index`.

Usage examples of real-world :term:`Lino applications <Lino application>`:

.. autosummary::
   :toctree:

   bs3
   public
   anna
   liina
   avanti1
   voga1

Examples used by a tutorial:

.. autosummary::
   :toctree:

   chooser
   min1
   mti
   nomti
   lets2
   actors
   actions
   tables
   dumps
   combo
   polls2
   watch
   watch2
   workflows


Poorly documented examples in early development stades:

.. autosummary::
   :toctree:

   min2
   belref
   estref


Sleeping:

.. autosummary::
   :toctree:

   events
   crl
   homeworkschool
   i18n

"""
