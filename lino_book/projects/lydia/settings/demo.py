# -*- coding: UTF-8 -*-
# Copyright 2016-2021 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)

import datetime

from ..settings import *


class Site(Site):
    is_demo_site = True
    the_demo_date = datetime.date(2015, 5, 23)
    languages = "en de fr"
    use_ipdict = True

    # legacy_data_path = '...'

    def get_installed_plugins(self):
        yield super(Site, self).get_installed_plugins()
        yield 'lino.modlib.search'


SITE = Site(globals())
DEBUG = True

# SITE.plugins.tim2lino.configure(
#     languages='de fr',
#     timloader_module='lino_xl.lib.tim2lino.spzloader',
#     dbf_table_ext='.FOX',
#     #use_dbf_py=True,
#     use_dbfread=True)
