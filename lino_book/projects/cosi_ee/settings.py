# -*- coding: UTF-8 -*-
# Copyright 2014-2024 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)
"""
A :ref:`cosi` site for a small company in Estonia.

"""

from lino_cosi.lib.cosi.settings import *


class Site(Site):
    languages = 'en et'

    demo_fixtures = ['std', 'minimal_ledger',
    'furniture', 'demo', 'demo_bookings', 'payments', 'demo2', 'checkdata']

    is_demo_site = True
    # ignore_dates_after = datetime.date(2019, 05, 22)
    the_demo_date = 20240612
    default_ui = 'lino_react.react'

    def get_plugin_configs(self):
        yield super().get_plugin_configs()
        yield ('vat', 'declaration_plugin', 'lino_xl.lib.eevat')
        yield ('contacts', 'demo_region', "EE")
        yield ('countries', 'hide_region', False)
        yield ('countries', 'country_code', 'EE')
        yield ('countries', 'full_data', True)
        yield ('accounting', 'use_pcmn', True)
        yield ('accounting', 'start_year', 2023)
        yield ('help', 'make_help_pages', True)


SITE = Site(globals())
DEBUG = True
