from lino.mixins import Human
from lino.mixins.human import Born


class Person(Human, Born):
    pass
