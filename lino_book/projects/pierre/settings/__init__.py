# -*- coding: UTF-8 -*-
# Copyright 2014-2020 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)
"""
Default settings for a :ref:`cosi` site "à la Pierre".

"""

from lino_cosi.lib.cosi.settings import *


class Site(Site):
    languages = 'fr en'
    # demo_fixtures = 'std few_countries minimal_ledger euvatrates \
    # furniture demo demo_bookings demo2'.split()
    demo_fixtures = 'std few_countries minimal_ledger \
    furniture demo demo_bookings demo2 checkdata'.split()

    #def get_installed_plugins(self):
    #    yield super(Site, self).get_installed_plugins()
    #    yield 'lino_xl.lib.bevat'

    def get_plugin_configs(self):
        yield super().get_plugin_configs()
        # yield ('vat', 'declaration_plugin', 'lino_xl.lib.bevat')
        yield ('contacts', 'demo_region', 'BE')
        yield ('countries', 'hide_region', True)
        yield ('countries', 'country_code', 'BE')
        yield ('accounting', 'use_pcmn', True)
        # yield ('vat', 'use_online_check', True)  # doctest docs/topics/vies.rst
        # yield ('accounting', 'worker_model', 'contacts.Person')

    def get_plugin_modifiers(self, **kw):
        # disable invoicing
        kw = super().get_plugin_modifiers(**kw)
        kw.update(invoicing=None)
        return kw
