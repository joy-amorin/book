# -*- coding: UTF-8 -*-
# Copyright 2015-2018 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)
"""
A readonly user interface to :mod:`team
<lino_book.projects.noi1e>`.

See :ref:`noi.specs.bs3`.

.. autosummary::
   :toctree:

   settings
   tests
"""
