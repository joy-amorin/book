# -*- coding: UTF-8 -*-
# Copyright 2017 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)
"""Shows :ref:`vilma`, a village management tool.

.. autosummary::
   :toctree:

   settings

"""
