# -*- coding: UTF-8 -*-
# Copyright 2013-2016 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)
"""
This is the main module of the :ref:`belref` project.

.. autosummary::
   :toctree:

   settings
   settings.demo
   settings.doctests


"""
