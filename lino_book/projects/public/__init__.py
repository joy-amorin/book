# -*- coding: UTF-8 -*-
# Copyright 2015-2016 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)
"""
Another readonly user interface to :mod:`team
<lino_book.projects.noi1e>`.

See :ref:`noi.specs.public`.

.. autosummary::
   :toctree:

   settings
   tests
"""
