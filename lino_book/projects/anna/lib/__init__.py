# -*- coding: UTF-8 -*-
# Copyright 2016 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)
"""Fixtures specific for Lino Care.

.. autosummary::
   :toctree:

   tickets


"""

from lino_xl.lib.tickets import *
